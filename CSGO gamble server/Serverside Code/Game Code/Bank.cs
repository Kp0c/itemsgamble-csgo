﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rock_paper_scissors
{
    /// <summary>
    /// Работа с монетами игроков
    /// </summary>
    class Bank
    {
        /// <summary>
        /// количество монет в банке
        /// </summary>
        public int value = 0;


        /// <summary>
        /// Добавить монет
        /// </summary>
        /// <param name="player">Игрок, кому добавить монеты.</param>
        /// <param name="money">Количество монет.</param>
        public void AddMoney(Player player, int money)
        {
            ChangeMoney(player, money);
        }

        /// <summary>
        /// Выплатить награду победителю
        /// </summary>
        /// <param name="player">Обьект победителя</param>
        public void GiveReward(Player player)
        {
            AddMoney(player, value);
            value = 0;
        }

        /// <summary>
        /// Снять монеты
        /// </summary>
        /// <param name="player">Игрок, у которого снять монеты.</param>
        /// <param name="money">Количество монет.</param>
        public void WithdrawMoney(Player player, int money)
        {
            ChangeMoney(player, -money);
        }

        /// <summary>
        /// Изменить количество монет.
        /// </summary>
        /// <param name="player">Игрок, кому изменить монеты.</param>
        /// <param name="money">Количество монет.</param>
        private void ChangeMoney(Player player, int money)
        {
            object q;
            player.PlayerObject.Set("money", player.PlayerObject.GetInt("money") + money);
            if (!player.PlayerObject.TryGetValue("serverMoney", out q))
            {
                player.PlayerObject.Set("serverMoney", 0);
                player.PlayerObject.Save();
            }
            player.PlayerObject.Set("serverMoney", player.PlayerObject.GetInt("serverMoney") + money);
            player.PlayerObject.Save();
        }

        private int GetTax(int bank)
        {
            //TODO: release it
            return bank;
        }
    }
}
