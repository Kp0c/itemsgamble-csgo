﻿using PlayerIO.GameLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rock_paper_scissors
{
    class Log
    {
        DatabaseArray log = new DatabaseArray();

        DatabaseObject game = new DatabaseObject();

        public void Init(BigDB db)
        {
            db.LoadRange("GameLogs","ById",null,null,null,1,delegate(DatabaseObject[] obj) {
                game = new DatabaseObject();
                int id = obj[0].GetInt("id")+1;
                game.Set("time",DateTime.UtcNow.ToString().Replace(" ","___").Replace(":","_").Replace(".","_"));
                game.Set("id", id);

                db.CreateObject("GameLogs", game.GetString("time"), game,
                    (DatabaseObject dbobj) => { Write("Init log with id " + dbobj.GetInt("id")); }, Error);

            },Error);
        }

        public void Error(PlayerIOError error)
        {
            Console.WriteLine(error.Message);
        }

        public void Write(string message)
        {
            log.Add(message);
        }

        public void Save(BigDB db)
        {
            db.Load("GameLogs", game.GetString("time"), (DatabaseObject obj) =>
            {
                obj.Set("log", log);
                obj.Save();
                log.Clear(); ;
                Console.WriteLine(obj.GetInt("id") + "saved");
            }, Error);
        }
    }
}
