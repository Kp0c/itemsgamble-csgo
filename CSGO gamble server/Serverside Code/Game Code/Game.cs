﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using PlayerIO.GameLibrary;
using System.Drawing;

namespace Rock_paper_scissors {

	public class Player : BasePlayer {
        public bool ready = false;
        public int symbol = -1;
        public int wins = 0;

        public void Refresh()
        {
            ready = false;
            symbol = -1;
            wins = 0;
        }
	}

    [RoomType("Rock-paper-scissors")]
	public class GameCode : Game<Player> {

        public List<Player> players = new List<Player>();
        public const int TIME_TO_CHOOSE = 30500;
        public const int TIME_TO_APPLY = 10500;

        public List<Timer> needsToStopTimers = new List<Timer>();

        Bank bank = new Bank();
        Log log = new Log();

        Timer applyTimer;
        Timer chooseTimer;

        Timer sendTimeToApplyTimer;
        int applyTimeRemaining = 0;

        Timer sendTimeToChooseTimer;
        int chooseTimeRemaining = 0;

        bool gameRunning = false;

        public override void GameStarted()
        {
            log.Init(PlayerIO.BigDB);
            PreloadPlayerObjects = true;
            PreloadPayVaults = true;
        }

		public override void GameClosed() {
            log.Write("Room " + RoomId + " with name "+ RoomData["name"] + " closed");
            log.Save(PlayerIO.BigDB);
			Console.WriteLine("RoomId: " + RoomId);
		}

		public override void UserJoined(Player player) {
            log.Write(player.ConnectUserId + " connected. Name: "+ player.JoinData["name"]);
            player.Send("Chat", "(Server) Привет :)");
            player.Refresh();
            player.Send("RoomInfo", @"<b>" + RoomData["name"] + @"</b>", RoomData["bet"], RoomData["bestOf"]);
            players.Add(player);
            if (players.Count == 2)
            {
                player.Send("OpponentInfo", players[0].JoinData["name"], players[0].JoinData["avatar"]);
                players[0].Send("OpponentInfo", player.JoinData["name"], player.JoinData["avatar"]);
                Visible = false;

                ApplyTimer();
            }
		}

        public void ApplyTimer()
        {
            if (sendTimeToApplyTimer != null) sendTimeToApplyTimer.Stop();

            applyTimeRemaining = TIME_TO_APPLY - 500;

            Broadcast("TimeToApply",applyTimeRemaining);

            sendTimeToApplyTimer = AddTimer(() =>
            {
                applyTimeRemaining -= 1000;
                Broadcast("TimeToApply", applyTimeRemaining);
            }, 1000);

            applyTimer = ScheduleCallback(() =>
            {
                sendTimeToApplyTimer.Stop();
                bool leaved = false;
                List<Player> playersToDc = new List<Player>();
                for (int i = 0; i < players.Count; i++)
                {
                    if (!players[i].ready)
                    {
                        Broadcast("Chat", players[i].JoinData["name"] + " не готов :(");
                        playersToDc.Add(players[i]);
                        leaved = true;
                    }
                }

                playersToDc.ForEach(p => p.Disconnect());

                if (leaved)
                {
                    foreach (Player p in players)
                    {
                        p.Refresh();
                    }
                }
            }, TIME_TO_APPLY);
        }

        public override bool AllowUserJoin(Player player)
        {
            int maxplayers; 
            if (!int.TryParse(RoomData["maxPlayers"], out maxplayers))
            {
                maxplayers = 2;
                Console.WriteLine("default");
            }

            if (players.Count < maxplayers)
            {
                return true;
            }
            log.Write("Disallow connnect to " + player.ConnectUserId + ". Name: " + player.JoinData["name"]);
            Console.WriteLine("Слишком много игроков :(");
            return false;
        } 

		public override void UserLeft(Player player) {
            if (players.Count == 2)
            {
                Player opponent = players.Find(p => p.Id != player.Id);

                RoomData["creator"] = opponent.JoinData["name"];
                RoomData.Save();
                if (applyTimer != null) applyTimer.Stop();
                if (sendTimeToApplyTimer != null) sendTimeToApplyTimer.Stop();
                if (sendTimeToChooseTimer != null) sendTimeToChooseTimer.Stop();
                if (chooseTimer != null) chooseTimer.Stop();

                if (gameRunning)
                {
                    WinnerOperations(opponent);
                    opponent.Send("Chat", player.JoinData["name"] + " получил тех. поражение");
                    log.Write(player.ConnectUserId + " получил тех. поражение. Name: " + player.JoinData["name"]);
                    gameRunning = false;
                    opponent.Send("Result", 1);
                }

                opponent.Send("PrepareToNewGame");
                opponent.Send("OpponentLeave");
                opponent.Send("Chat", player.JoinData["name"] + " вышел :(");
                log.Write(player.ConnectUserId + " вышел :( Name: " + player.JoinData["name"]);
                opponent.ready = false;
                opponent.symbol = -1;
                opponent.wins = 0;
                Visible = true;
            }
            players.Remove(player);

            needsToStopTimers.ForEach(timer => timer.Stop());
            needsToStopTimers.Clear();
		}

        public override void GotMessage(Player player, Message message)
        {
            switch (message.Type)
            {
                case "Chat":
                    Broadcast("Chat", player.JoinData["name"] + ": " + message.GetString(0));
                    log.Write("Chat: "+player.JoinData["name"] + ": " + message.GetString(0));
                    break;
                case "Ready":
                    Player opponent = players.Find(p => p.Id != player.Id);
                    if (opponent != null) opponent.Send("OpponentReady");
                    player.ready = true;
                    if (player.ready && opponent != null && opponent.ready)
                    {
                        log.Write("Both players ready");
                        bank.WithdrawMoney(players[0], Convert.ToInt32(RoomData["bet"]));
                        bank.WithdrawMoney(players[1], Convert.ToInt32(RoomData["bet"]));
                        bank.value += Convert.ToInt32(RoomData["bet"]) * 2;
                        StartRound();
                    }
                    break;
                case "ApplySymbol":
                    log.Write("Player "+player.ConnectUserId+" set "+message.GetInt(0));
                    player.symbol = message.GetInt(0);
                    if (players[0].symbol >= 0 && players[1].symbol >= 0)
                    {
                        if (chooseTimer != null) chooseTimer.Stop();
                        if (sendTimeToChooseTimer != null) sendTimeToChooseTimer.Stop();
                        SendResult();
                    }
                    break;
                default:
                    Console.WriteLine(message.ToString());
                    log.Write("UNKNOWN MESSAGE: " + player.ConnectUserId + ". Type: " + message.Type + ". Message:" + message.ToString());
                    break;
            }
        }

        public void StartRound()
        {
            Console.WriteLine("Start round");
            log.Write("Start new round");
            
            if (applyTimer != null) applyTimer.Stop();
            if (sendTimeToApplyTimer != null) sendTimeToApplyTimer.Stop();

            object q = (object) 0;
            players[0].PlayerObject.TryGetValue("serverMoney", out q);
            if ((int)q < Convert.ToInt16(RoomData["bet"]))
            {
                Broadcast("Chat", "У " + players[0].JoinData["name"] + " недостаточно монет для игры");
                log.Write(players[0].ConnectUserId+" havent money for the game");
                players[0].Disconnect();
                return;
            }

            if (players.Count > 1)
            {
                q = (object)0;
                players[1].PlayerObject.TryGetValue("serverMoney", out q);
                if ((int)q < Convert.ToInt16(RoomData["bet"]))
                {
                    Broadcast("Chat", "У " + players[1].JoinData["name"] + " недостаточно монет для игры");
                    log.Write(players[1].ConnectUserId + " havent money for the game");
                    players[1].Disconnect();
                    return;
                }
    
                gameRunning = true;
                Broadcast("GameStart");

                chooseTimeRemaining = TIME_TO_CHOOSE - 500;

                Broadcast("TimeToChoose", chooseTimeRemaining);

                if (sendTimeToChooseTimer != null) sendTimeToChooseTimer.Stop();
                if (chooseTimer != null) chooseTimer.Stop();

                sendTimeToChooseTimer = AddTimer(() =>
                {
                    chooseTimeRemaining -= 1000;
                    Broadcast("TimeToChoose", chooseTimeRemaining);
                }, 1000);

                chooseTimer = ScheduleCallback(() =>
                {
                    List<Player> dontChoosePlayers = new List<Player>();
                    foreach (Player p in players)
                    {
                        if (p.symbol == -1)
                        {
                            dontChoosePlayers.Add(p);
                        }
                    }

                    SendResult();

                    foreach (Player p in dontChoosePlayers)
                    {
                        p.Disconnect();
                    }
                }, TIME_TO_CHOOSE);
            }
        }

        public void SendResult()
        {
            int winner = GetWinner(players[0], players[1]); //-1 - Tie, 0-players[0] win, 1 - players[1] win

            if (winner == -1) log.Write("Round tie"); else log.Write("Winner of round is " + players[winner].ConnectUserId);

            if (winner == -1)
            {
                Console.WriteLine("Tie");
                players[0].Send("RoundWin", -1, players[0].symbol, players[1].symbol);
                players[1].Send("RoundWin", -1, players[1].symbol, players[0].symbol);
            }
            else if (winner == 0)
            {
                Console.WriteLine(players[0].JoinData["name"] + " win");
                players[0].Send("RoundWin", 1, players[0].symbol, players[1].symbol);
                players[0].wins += 1;
                players[1].Send("RoundWin", 0, players[1].symbol, players[0].symbol);
            }
            else
            {
                Console.WriteLine(players[1].JoinData["name"] + " win");
                players[1].Send("RoundWin", 1, players[1].symbol, players[0].symbol);
                players[1].wins += 1;
                players[0].Send("RoundWin", 0, players[0].symbol, players[1].symbol);
            }

            bool startNewGame = true;
            ForEachPlayer(p =>
            {
                p.ready = false;
                p.symbol = -1;
                if (p.wins > Convert.ToDouble(RoomData["bestOf"]) / 2)
                {
                    WinnerOperations(p);

                    startNewGame = false;
                    gameRunning = false;
                    ForEachPlayer(player => player.wins = 0);
                }
                else if (p.wins == Convert.ToDouble(RoomData["bestOf"]) / 2 && p.wins == players.Find(pl => pl.Id != p.Id).wins)
                {
                    needsToStopTimers.Add(ScheduleCallback(() =>
                    {
                        ApplyTimer();
                        Broadcast("Result", -1);
                    }, 5000));
                    startNewGame = false;
                    gameRunning = false;
                    ForEachPlayer(player => player.wins = 0);
                }
            });

            if (startNewGame)  needsToStopTimers.Add(ScheduleCallback(() => StartRound(), 5000));
        }

        public void WinnerOperations(Player winner)    
        {
            Player opponent = players.Find(p => p.Id != winner.Id);

            log.Write("Give reward for winner "+winner.ConnectUserId+" with value "+bank.value);
            bank.GiveReward(winner);

            needsToStopTimers.Add(ScheduleCallback(() =>
            {
                ApplyTimer();
                winner.Send("Result", 1);
                if (opponent != null) opponent.Send("Result", 0);
            }, 5000));
        }
        

        public int GetWinner(Player player1, Player player2)
        {
            if (player1.symbol == player2.symbol) return -1;

            if (player1.symbol == -1) return 1;
            if (player2.symbol == -1) return 0;

            if (Math.Abs(player1.symbol - player2.symbol) == 1)
            {
                if (player1.symbol < player2.symbol) return 0; else return 1;
            }
            else if (Math.Abs(player1.symbol - player2.symbol) == 2)
            {
                if (player1.symbol > player2.symbol) return 0; else return 1;
            }

            return -1;
        }
	}
}