﻿using PlayerIO.GameLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotRoom
{
    public class Player : BasePlayer
    {

    }

    [RoomType("BotRoom")]
    public class Game : Game<Player>
    {
        public override void GameStarted()
        {
            //this.PreloadPlayerObjects = true;
            //this.PreloadPayVaults = true;
        }

        public override void UserJoined(Player player)
        {
            RoomData["BotsCount"] = (Convert.ToInt32(RoomData["BotsCount"]) + 1).ToString();
            RoomData.Save();
        }

        public override void UserLeft(Player player)
        {
            RoomData["BotsCount"] = (Convert.ToInt32(RoomData["BotsCount"]) - 1).ToString();

            RoomData.Save();
        }
    }
}
