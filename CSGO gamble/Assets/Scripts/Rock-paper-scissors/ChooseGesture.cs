﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChooseGesture : MonoBehaviour {

    public Button accept;
    public Button rock;
    public Button scissors;
    public Button paper;
    public Text chooseTimer;
	// Use this for initialization
	void Start () {
        rock.onClick.AddListener(() => GameCode.instance.SetSymbol(0));
        scissors.onClick.AddListener(() => GameCode.instance.SetSymbol(1));
        paper.onClick.AddListener(() => GameCode.instance.SetSymbol(2));
        accept.onClick.AddListener(() => GameCode.instance.ApplySymbol());

	}
	
	// Update is called once per frame
	void Update () {
        chooseTimer.text = GameCode.instance.TimeToChoose.ToString();
	}
}
