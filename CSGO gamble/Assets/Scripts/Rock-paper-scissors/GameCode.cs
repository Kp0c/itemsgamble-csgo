﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PlayerIOClient;
using UnityEngine.EventSystems;

public class GameCode : MonoBehaviour {
    [HideInInspector]
    public int timeToApply, TimeToChoose;
    [HideInInspector]
    public Status playerStatus = Status.Wait, opponentStatus = Status.Wait;

    [HideInInspector]
    public int winner = -1;

    [Header("Players")]
    public Text playerName;
    public Text opponentName;
    public Image playerAvatar;
    public Image opponentAvatar;

    [Header("ready panel")]
    public GameObject readyPanelPref;
    GameObject readyPanel;

    [Header("choose panel")]
    public GameObject choosePanelPref;
    GameObject choosePanel;

    [Header("winner panel")]
    public GameObject winnerPanelPref;
    GameObject winnerPanel;

    [Header("Win round")]
    public GameObject playerRoundsWin;
    public GameObject opponentRoundsWin;

    public GameObject roundWinPref;
    public GameObject roundLoosePref;
    public GameObject roundTiePref;

    public GameObject roundWinnerPanelPref;
    GameObject roundWinnerPanel;
    public Sprite rock;
    public Sprite scissors;
    public Sprite paper;

    [Space(20)]

    public Text status;
    public Text roomName;
    
    public GameObject waitInfoPanel;
    [Header("In-game UI")]

    [Header("Other")]
    public Sprite noAvatar;
    public GameObject leavePanelPref;
    GameObject leavePanel;

    [HideInInspector]
    static public GameCode instance;

    void Awake()
    {
        instance = this;
    }

	void Start () {
        playerName.text = Connection.instance.user.personaname;
        StartCoroutine(DownloadAvatar(Connection.instance.user.avatarfull, playerAvatar));
	}

    bool once = true;
	void Update () {
        if (Connection.instance.connection == null)
        {
            status.text = "Подождите пока подсоединимся";
            return;
        }
        if (once && Connection.instance.connection.Connected)
        {
            once = false;
            status.text = "Подключено";
            Connection.instance.connection.AddOnMessage(OnMessage);
            Connection.instance.connection.AddOnDisconnect(OnDisconnect);
        }
	}

    public void OnMessage(object sender,Message message)
    {
        Debug.Log(message.ToString());
        switch (message.Type)
        {
            case "Chat": Debug.Log("Get chat message"); break;

            case "OpponentInfo":
                Debug.Log("Get opponent info");
                waitInfoPanel.SetActive(false);

                opponentAvatar.gameObject.SetActive(true);
                opponentName.gameObject.SetActive(true);
                opponentName.text = message.GetString(0);
                StartCoroutine(DownloadAvatar(message.GetString(1), opponentAvatar));

                opponentStatus = Status.Wait;
                playerStatus = Status.Wait;

                readyPanel = Instantiate(readyPanelPref) as GameObject;
                readyPanel.transform.SetParent(GameObject.Find("Canvas").transform, false);
                if (readyPanel == null) Debug.Log("ready panel on");
                if (winnerPanel != null) Destroy(winnerPanel);
                break;

            case "OpponentLeave":
                waitInfoPanel.SetActive(true);

                opponentName.gameObject.SetActive(false);
                opponentAvatar.gameObject.SetActive(false);
                opponentName.text = "Имя противника";
                opponentAvatar.sprite = noAvatar;

                if (winnerPanel != null)
                {
                    StartCoroutine(DestroyAfter(3f, winnerPanel));
                    Destroy(winnerPanel.transform.Find("Timer").GetComponent<Text>().gameObject);
                }

                if (readyPanel != null) Destroy(readyPanel);

                break;

            case "RoomInfo":
                roomName.text = message.GetString(0)+", ставка: "+message.GetString(1)+", лучший с "+message.GetString(2);
                break;

            case "GameStart":
                Debug.Log("Start game");
                if (choosePanel != null) Destroy(choosePanel);
                if (winnerPanel != null) Destroy(winnerPanel);
                if (readyPanel != null) Destroy(readyPanel);
                break;

            case "OpponentReady":
                opponentStatus = Status.Ready;
                break;

            case "TimeToApply":
                timeToApply = message.GetInt(0) / 1000;
                break;

            case "PrepareToNewGame":
                if (readyPanel != null) Destroy(readyPanel);

                waitInfoPanel.SetActive(true);

                if (choosePanel != null) Destroy(choosePanel);
                break;

            case "TimeToChoose":
                if (choosePanel == null)
                {
                   choosePanel = Instantiate(choosePanelPref) as GameObject;
                   choosePanel.transform.SetParent(GameObject.Find("Canvas").transform, false);
                }
                TimeToChoose = message.GetInt(0) / 1000;
                break;

            case "Result":
                Debug.Log("GetResult");
                if (choosePanel != null) Destroy(choosePanel);
                winnerPanel = Instantiate(winnerPanelPref) as GameObject;
                winnerPanel.transform.SetParent(GameObject.Find("Canvas").transform, false);
                winner = message.GetInt(0);

                for (int i = 0; i < playerRoundsWin.transform.childCount; i++)
                {
                    Destroy(playerRoundsWin.transform.GetChild(i).gameObject);
                }
                for (int i = 0; i < opponentRoundsWin.transform.childCount; i++)
                {
                    Destroy(opponentRoundsWin.transform.GetChild(i).gameObject);
                }
                break;

            case "groupdisallowedjoin":
                Leave();
                Connection.instance.errors.Add("Извините, но комната yже полная");
                break;

            case "RoundWin":
                if (choosePanel != null) Destroy(choosePanel);
                if (message.GetInt(0) == 0)
                {
                    GameObject newGO = Instantiate(roundLoosePref) as GameObject;
                    newGO.transform.SetParent(playerRoundsWin.transform, false);

                    newGO = Instantiate(roundWinPref) as GameObject;
                    newGO.transform.SetParent(opponentRoundsWin.transform, false);
                }
                if (message.GetInt(0) == 1)
                {
                    GameObject newGO = Instantiate(roundWinPref) as GameObject;
                    newGO.transform.SetParent(playerRoundsWin.transform, false);

                    newGO = Instantiate(roundLoosePref) as GameObject;
                    newGO.transform.SetParent(opponentRoundsWin.transform, false);
                }
                if (message.GetInt(0) == -1)
                {
                    GameObject newGO = Instantiate(roundTiePref) as GameObject;
                    newGO.transform.SetParent(playerRoundsWin.transform, false);
                    newGO = Instantiate(roundTiePref) as GameObject;
                    newGO.transform.SetParent(opponentRoundsWin.transform, false);
                }

                roundWinnerPanel = Instantiate(roundWinnerPanelPref) as GameObject;
                roundWinnerPanel.transform.SetParent(GameObject.Find("Canvas").transform, false);

                roundWinnerPanel.transform.FindChild(@"Players/PlayerAvatar").GetComponent<Image>().sprite = playerAvatar.sprite;
                roundWinnerPanel.transform.FindChild(@"Players/OpponentAvatar").GetComponent<Image>().sprite = opponentAvatar.sprite;

                roundWinnerPanel.transform.FindChild(@"Players/PlayerName").GetComponent<Text>().text = playerName.text;
                roundWinnerPanel.transform.FindChild(@"Players/OpponentName").GetComponent<Text>().text = opponentName.text;

                switch (message.GetInt(1))
                {
                    case 0: roundWinnerPanel.transform.FindChild("PlayerGesture").GetComponent<Image>().sprite = rock; break;
                    case 1: roundWinnerPanel.transform.FindChild("PlayerGesture").GetComponent<Image>().sprite = scissors; break;
                    case 2: roundWinnerPanel.transform.FindChild("PlayerGesture").GetComponent<Image>().sprite = paper; break;
                }

                switch (message.GetInt(2))
                {
                    case 0: roundWinnerPanel.transform.FindChild("OpponentGesture").GetComponent<Image>().sprite = rock; break;
                    case 1: roundWinnerPanel.transform.FindChild("OpponentGesture").GetComponent<Image>().sprite = scissors; break;
                    case 2: roundWinnerPanel.transform.FindChild("OpponentGesture").GetComponent<Image>().sprite = paper; break;
                }

                StartCoroutine(DestroyAfter(5f, roundWinnerPanel));
                break;

            default:
                print("Unknow message: "+message.ToString());
                break;
        }
    }

    IEnumerator DestroyAfter(float delay, GameObject gameObject)
    {
        yield return new WaitForSeconds(delay);
        if(gameObject!=null) Destroy(gameObject);
    }

    public void OnDisconnect(object sender,string reason)
    {
        Connection.instance.connection = null;
        Leave();
    }

    public void SetReady()
    {
        playerStatus = Status.Ready;
        Connection.instance.connection.Send("Ready");
    }

    int symbol = -1;
    public void SetSymbol(int symbol)
    {
        this.symbol = symbol;
    }

    public void ApplySymbol()
    {
        Connection.instance.connection.Send("ApplySymbol", symbol);
    }

    public void TryLeave()
    {
        if (leavePanel == null)
        {
            leavePanel = Instantiate(leavePanelPref) as GameObject;
            leavePanel.transform.SetParent(GameObject.Find("Canvas").transform, false);
        }
    }

    public void Leave()
    {
        Connection.instance.state = Connection.State.Connected;
        Application.LoadLevel("MainMenu");
    }

    void OnDestroy()
    {
        Connection.instance.connection.OnMessage -= OnMessage;
        Connection.instance.connection.Disconnect();
        Connection.instance.connection.OnDisconnect -= OnDisconnect;
    }

    IEnumerator DownloadAvatar(string url,Image avatar)
    {
        WWW www = new WWW(url);
        yield return www;
        avatar.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
    }
}

public enum Status
{
    Wait,
    Ready,
    Decline,
}
