﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Winner : MonoBehaviour {

    public Text Timer;
    public Text result;
    public Text winnerNickname;
    public Image winnerAvatar;


    public Button leave;
    public Button retry;


	// Use this for initialization
    void Start()
    {
        leave.onClick.AddListener(() => GameCode.instance.TryLeave());
        retry.onClick.AddListener(() => GameCode.instance.SetReady());
        if (GameCode.instance.winner == -1)
        {
            winnerAvatar.gameObject.SetActive(false);
            winnerNickname.gameObject.SetActive(false);
            result.text = "Ничья";
        }
        if (GameCode.instance.winner == 0)
        {
            result.text = "Извините, но вы проиграли. Победитель...";

            winnerAvatar.sprite = GameObject.Find("OpponentAvatar").GetComponent<Image>().sprite;
            winnerNickname.text = GameObject.Find("OpponentName").GetComponent<Text>().text;
        }
        if (GameCode.instance.winner == 1)
        {
            result.text = "Поздравляем! Победитель...";

            winnerAvatar.sprite = GameObject.Find("PlayerAvatar").GetComponent<Image>().sprite;
            winnerNickname.text = GameObject.Find("PlayerName").GetComponent<Text>().text;
        }
    }
	
	// Update is called once per frame
	void Update () {
        Timer.text = GameCode.instance.timeToApply.ToString();
	}
}
