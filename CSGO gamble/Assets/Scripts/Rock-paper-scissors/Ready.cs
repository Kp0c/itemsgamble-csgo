﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PlayerIOClient;

public class Ready : MonoBehaviour {

    public GameObject readyPanel;
    public Text applyTimer;
    public Sprite statusReady;
    public Sprite statusWait;
    public Sprite statusDecline;

    public Button readyButton;
    [HideInInspector]
    public Image playerStatus;
    [HideInInspector]
    public Image opponentStatus;
    void Start()
    {
        playerStatus = GameObject.Find("PlayerAvatar").transform.FindChild("Status").GetComponent<Image>();
        opponentStatus = GameObject.Find("OpponentAvatar").transform.FindChild("Status").GetComponent<Image>();
        playerStatus.gameObject.SetActive(true);
        opponentStatus.gameObject.SetActive(true);

        readyButton.onClick.AddListener(() =>
        {
            GameCode.instance.SetReady();
        });
    }

    void Update()
    {
        applyTimer.text = GameCode.instance.timeToApply.ToString();
            playerStatus.gameObject.SetActive(true);
            opponentStatus.gameObject.SetActive(true);
            switch (GameCode.instance.playerStatus)
            {
                case Status.Wait:
                    playerStatus.sprite = statusWait;
                    break;
                case Status.Ready:
                    playerStatus.sprite = statusReady;
                    break;
                case Status.Decline:
                    playerStatus.sprite = statusDecline;
                    break;
                default:
                    Debug.LogError("Are it really happened??? Check playerStatus, please!");
                    break;
            }

            switch (GameCode.instance.opponentStatus)
            {
                case Status.Wait:
                    opponentStatus.sprite = statusWait;
                    break;
                case Status.Ready:
                    opponentStatus.sprite = statusReady;
                    break;
                case Status.Decline:
                    opponentStatus.sprite = statusDecline;
                    break;
                default:
                    Debug.LogError("Are it really happened??? Check opponentStatus, please!");
                    break;
            }
    }

    void OnDestroy()
    {
        playerStatus.gameObject.SetActive(false);
        opponentStatus.gameObject.SetActive(false);
    }
}
