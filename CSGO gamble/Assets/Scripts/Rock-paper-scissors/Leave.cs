﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Leave : MonoBehaviour {

    public Button acceptLeave;
    public Button declineLeave;

	void Start () {
        acceptLeave.onClick.AddListener(() => GameCode.instance.Leave());
        declineLeave.onClick.AddListener(() => Destroy(gameObject));
	}
}
