﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class UIWindowBase : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
        RectTransform m_transform = null;
        public Anchore anchore;
        public List<GameObject> m_targetDragObjects = new List<GameObject>();
        private bool _isDragging = false;

        public enum Anchore
        {
            left_bottom,
            right_bottom
        }
   
        // Use this for initialization
        void Start ()
        {
                m_transform = GetComponent<RectTransform> ();
        }
   
        public void OnDrag (PointerEventData eventData)
        {
            if (_isDragging)
            {
                m_transform.position += new Vector3(eventData.delta.x, eventData.delta.y);
                Limit(m_transform);
            }
        }
        public void OnBeginDrag(PointerEventData eventData)
        {

            if (eventData.pointerCurrentRaycast.gameObject == null)
                return;

           /* if (eventData.pointerCurrentRaycast.gameObject.name == m_targetDragObject.name)
                _isDragging = true;*/
            if(m_targetDragObjects.Contains(eventData.pointerCurrentRaycast.gameObject))
                _isDragging = true;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _isDragging = false;
        }

        RectTransform Limit(RectTransform lim)
        {
            switch (anchore)
            {
                case Anchore.left_bottom:
                    lim.anchoredPosition = new Vector2(Mathf.Clamp(lim.anchoredPosition.x, 0, Screen.width - lim.sizeDelta.x),
                        Mathf.Clamp(lim.anchoredPosition.y, 0, Screen.height - lim.sizeDelta.y));
                    break;
                case Anchore.right_bottom:
                    lim.anchoredPosition = new Vector2(Mathf.Clamp(lim.anchoredPosition.x, -Screen.width + lim.sizeDelta.x, 0),
                Mathf.Clamp(lim.anchoredPosition.y, 0, Screen.height - lim.sizeDelta.y));
                    break;
            }
            return lim;
        }
}