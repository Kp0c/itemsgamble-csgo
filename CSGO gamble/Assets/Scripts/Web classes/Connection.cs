﻿using UnityEngine;
using System.Collections;
using PlayerIOClient;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Threading;

public class Connection : MonoBehaviour {

    public bool DEBUG = false;
    public bool ServerDEBUG = false;
    public string serverDEBUG_IP = "127.0.0.1";


    public List<string> errors = new List<string>();

    public const string GAME_ID = "items-gumble-test-grbrbu1o0o8enubebcyw";
    public const string API_KEY = "810B76108EF6F454BE44181FA1FA251B";

    public Client player;
    public PlayerIOClient.Connection connection;

    [HideInInspector]
    public SteamUserInfo user = null;
    [HideInInspector]
    public string steamId;
    [HideInInspector]
    public string offerLink;

    public int money;
    public State state = State.Menu;

    public enum State
    {
        Menu,
        Wait,
        Connected,
        Rock_paper_scissors,
    }

    public static Connection instance;

#region TODO: add it if i fix web-build bug
   /* public void ConnectThroughSteam(string nickname)
    {
        logLoginField.text = nickname;
    }

    public void ConnectThroughSteam()
    {
        Application.ExternalCall("ConnectTroughSteam");
    }*/
#endregion


    void Start()
    {
        state = State.Menu;
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;
#if UNITY_EDITOR
        if (DEBUG)
        {
            Connect("Fruto","123");
        }
#endif
    }

    public void Connect(string nickname,string password)
    {
        if (nickname != "" && password != "")
        {
            key = nickname;
            PlayerIO.QuickConnect.SimpleConnect(GAME_ID, nickname, password, null, Connected, Error);
        }
    }

    public void Register(string login,string password,string steamId,string tradeLink)
    {
        if (login != "" && password != "" && steamId != "" && tradeLink != "")
        {
            key = login;
            this.steamId = steamId;
            this.offerLink = tradeLink;
            this.money = 0;
            PlayerIO.QuickConnect.SimpleRegister(GAME_ID, login, password, null, null, null, null, null, null, Registered, Error);
        }
        else
        {
            errors.Add("Заполните все поля.");
        }
    }

    IEnumerator SteamUserInfoCoroutine(string steamId)
    {
        float timeStart = Time.unscaledTime;
        user = new SteamUserInfo(API_KEY, steamId);
        while (user.personaname == "")
        {
            yield return new WaitForSeconds(0.3f);
        }
        Debug.Log("end get user info in "+(Time.unscaledTime-timeStart).ToString()+" seconds");
    }

    private void Registered(Client client)
    {
        StartCoroutine(SteamUserInfoCoroutine(steamId));

        client.BigDB.LoadMyPlayerObject((DatabaseObject obj) =>
        {
            obj.Set("steamId", steamId);
            obj.Set("money", 0);
            obj.Set("offerLink", offerLink);
            obj.Set("login", key);
            obj.Save();
        }, Error);

        Debug.Log("Зарегистрировано");
        player = client;
        state = State.Connected;
        if (Connection.instance.ServerDEBUG)
        {
            Debug.Log("Дебаг сервера включен");
            Connection.instance.player.Multiplayer.DevelopmentServer = new ServerEndpoint(serverDEBUG_IP, 8184);
        }
    }

    public string key = "";
    private void Connected(Client client)
    {
        client.BigDB.LoadMyPlayerObject((DatabaseObject result) =>
        {
            steamId = result.GetString("steamId");
            StartCoroutine(SteamUserInfoCoroutine(steamId));
            money = result.GetInt("money");
            offerLink = result.GetString("offerLink");
        }, Error);

        Debug.Log("Подсоединились");
        player = client;
        state = State.Connected;
        if (Connection.instance.ServerDEBUG)
        {
            Debug.Log("Дебаг сервера включен");
            Connection.instance.player.Multiplayer.DevelopmentServer = new ServerEndpoint(serverDEBUG_IP, 8184);
        }
    }

    public void Error(PlayerIOError error)
    {
        //regPanel.SetActive(true);
        switch (error.ErrorCode)
        {
            case ErrorCode.RoomAlreadyExists:
                errors.Add("Такая комната уже существует");
                Debug.Log("Такая комната уже существует");
                break;
            case ErrorCode.InvalidPassword:
                errors.Add("Неправильный пароль");
                state = State.Menu;
                break;
            case ErrorCode.UnknownUser:
                errors.Add("Неизвестный пользователь");
                state = State.Menu;
                //Register();
                break;
            case ErrorCode.InvalidRegistrationData:
                errors.Add("Логин занят");
                state = State.Menu;
                break;
            default:
                errors.Add(error.Message);
                Debug.LogError(error.ErrorCode);
                break;
        }
    }
}
