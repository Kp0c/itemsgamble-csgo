﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Collections.Generic;
using Boomlagoon.JSON;
using System.Text;
using System.Threading;
using System.IO;

public class SteamUserInfo
{
    public string personaname = "";
    public string avatarfull = "";

    public SteamUserInfo(string apiKey,string steamId)
    {
        Thread downloadInfo = new Thread(new ParameterizedThreadStart(DownloadInfo));
        downloadInfo.Start(new Info(apiKey,steamId));
    }

    public void DownloadInfo(object info)
    {
        
        string apiKey = ((Info)info).apiKey;
        string steamId = ((Info)info).steamId;


        WebRequest request = WebRequest.Create("http://tdb.host22.com/inGameGeters/getUser.php?key=" + apiKey + "&steamid=" + steamId);
        request.Timeout = 10000;
        request.Proxy = null;
        string responseFromServer = "";
        try
        {
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream dataStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        responseFromServer = reader.ReadToEnd();
                        response.Close();
                    }
                }
            }
        }
        catch (WebException ex)
        {
            Debug.Log(ex.Message);
            Debug.Log("Try again");
            DownloadInfo(info);
        }

        JSONObject jsonObject = new JSONObject(responseFromServer).GetField("response").GetField("players")[0];

        if (jsonObject != null)
        {
            personaname = jsonObject.GetField("personaname").str;
            avatarfull = jsonObject.GetField("avatarfull").str;
        }
        else
        {
            Debug.Log("JSON cannot be null");
        }
    }

    public override string ToString()
    {
        return "PersonaName = " + personaname + "; AvatarFull = " + avatarfull;
    }
}

class Info
{
    public string apiKey;
    public string steamId;

    public Info(string apiKey, string steamId)
    {
        this.apiKey = apiKey;
        this.steamId = steamId;
    }
}
