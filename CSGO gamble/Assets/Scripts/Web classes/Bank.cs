﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Boomlagoon.JSON;
using System.Threading;
using System;
using System.Text.RegularExpressions;
using System.IO;
using PlayerIOClient;

public static class Bank
{
    public static List<Item> items = new List<Item>();
    public static List<Item> itemsBot = new List<Item>();

    public static List<Thread> parseThreads = new List<Thread>();

    public static int countItems;

    public static JSONObject jsonObject;

    public static void DownloadInventory(object info)
    {
        string profile = ((BankInfo)info).profile;
        int appId = ((BankInfo)info).appId;
        int contextId = ((BankInfo)info).contextId;

        //WebRequest request = WebRequest.Create("http://steamcommunity.com/profiles/" + profile + "/inventory/json/" + appId + "/" + contextId + "/");
        WebRequest request = WebRequest.Create("http://tdb.host22.com/inGameGeters/getInventory.php?steamid=" + profile + "&appid=" + appId);
        request.Timeout = 10000;
        request.Proxy = null;
        string responseFromServer = "";
        try
        {
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream dataStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        responseFromServer = reader.ReadToEnd();
                        Debug.Log("downloaded inventory");
                        response.Close();
                    }
                }
            }
        }
        catch (WebException ex)
        {
            Debug.Log(ex.Message);
            Debug.Log("Try again");
            DownloadInventory(info);
        }

        jsonObject = new JSONObject(responseFromServer);

        if (jsonObject != null)
        {
            Thread t = new Thread(ParseItems);
			t.IsBackground = true;
            parseThreads.Add(t);
            t.Start();
        }
        else
        {
            Debug.Log("JSON cannot be null");
        }
    }

    public static void ParseItems()
    {
        JSONObject inventory = jsonObject.GetField("rgInventory");
        JSONObject desc = jsonObject.GetField("rgDescriptions");
        countItems = inventory.keys.Count;
        for (int i = 0; i < inventory.keys.Count; i++)
        {
            string classid = inventory.list[i].GetField("classid").str;
            string instanceid = inventory.list[i].GetField("instanceid").str;

            float tradable = desc.GetField(classid + "_" + instanceid).GetField("tradable").n;

            if (tradable > 0.5)
            {
                string name = desc.GetField(classid + "_" + instanceid).GetField("market_hash_name").str;
                string iconUrl = desc.GetField(classid + "_" + instanceid).GetField("icon_url").str;

				name = name.Replace(@"\u2605", "★");
                name = name.Replace(@"\u2122", "™");
                name = name.Replace(@"\u9f8d\u738b", "龍王");

				items.Add(new Item(name, "http://steamcommunity-a.akamaihd.net/economy/image/" + iconUrl, GetPrice(name),inventory.list[i].GetField("id").str));
				//items.Add(new Item(name, "http://tdb.host22.com/inGameGeters/getItemIcon.php?url=" + iconUrl, GetPrice(name),inventory.list[i].GetField("id").str));
            }
            else countItems--;
        }  
    }

    public static int GetPrice(string marketName, int appId = 730)
    {
        using (WebClient client = new WebClient())
        {
            string pattern = @"\b\d*[.]\d{2}?\b";
            Regex getPrice = new Regex(pattern);

            bool allow = false;
            string response = "";

            while (!allow)
                try
                {
                    //response = client.DownloadString("http://steamcommunity.com/market/priceoverview/?country=UA&currency=1&appid=" + appId + "&market_hash_name=" + marketName);
                    response = client.DownloadString("http://tdb.host22.com/inGameGeters/getItem.php?appid="+appId+"&name="+marketName);
                    allow = true;
                }
                catch (System.Net.WebException ex) { allow = false; Debug.Log(ex.Message); }

            JSONObject jsonObject = new JSONObject(response);
            string price = jsonObject.GetField("lowest_price").str;

           return Convert.ToInt32(Convert.ToDouble(getPrice.Match(price).Value) * 100);
        }
    }

    public static void DownloadItemsBot()
    {
        Connection.instance.player.BigDB.LoadRange("Items", "ById", null, null, null, 1000, Success);
    }

    public static void Success(DatabaseObject[] result)
    {
        foreach (DatabaseObject obj in result)
        {
            itemsBot.Add(new Item(obj.GetString("name"), obj.GetString("iconUrl"), obj.GetInt("price"), obj.Key));
        }
    }


}

public class InvItem
{
    public string class_id;
    public string instance_id;
}

public class BankInfo
{
    public string profile;
    public int appId;
    public int contextId;

    public BankInfo(string profile, int appId, int contextId)
    {
        this.profile = profile;
        this.appId = appId;
        this.contextId = contextId;
    }
}

public class Item
{
    public string name;
    public string iconUrl;
    public int price;
    public string id;

    public Item(string name, string iconUrl,int price,string id)
    {
        this.name = name;
        this.iconUrl = iconUrl;
        this.price = price;
        this.id = id;
    }

    
}
