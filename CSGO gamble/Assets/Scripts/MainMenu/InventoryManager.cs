﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PlayerIOClient;
using System;
using UnityEngine.EventSystems;
using System.Threading;

public class InventoryManager : MonoBehaviour {

    //Donate screen
    public Transform invContent;
    public Transform sendContent;
    public GameObject waitGetItemPanel;
    public Text waitGetItemText;
    public Button waitGetItemButton;
    public Button backButton;

    public GameObject itemPrefab;
    public Image progressBar;
    public Scrollbar scrollBar;

    public Text totalPrice;

    int totPrice;
    int downloadedItems = 0;
    string offerId = "";

    public void getedItem(Item item)
    {
        GameObject itemInst = GameObject.Instantiate(itemPrefab) as GameObject;
        itemInst.name = item.id;
        itemInst.transform.FindChild("Name").GetComponent<Text>().text = item.name;
        itemInst.transform.FindChild("Price").GetComponent<Text>().text = item.price.ToString();
        itemInst.GetComponent<Button>().onClick.AddListener(() => {
            if (itemInst.transform.parent == invContent)
            {
                itemInst.transform.SetParent(sendContent, false);
                totPrice += item.price;
                totalPrice.text = "Общая цена: " + totPrice.ToString();
            }
            else
            {
                itemInst.transform.SetParent(invContent, false);
                totPrice -= item.price;
                totalPrice.text = "Общая цена: " + totPrice.ToString();
            }
        });
        itemInst.transform.SetParent(invContent.transform, false);
        downloadedItems++;
        StartCoroutine(DownloadIcon(item.iconUrl, itemInst));
    }

    IEnumerator DownloadIcon(string iconUrl, GameObject item)
    {
        //TODO: download image from my site
        WWW www = new WWW(iconUrl);
		yield return www;
		Debug.Log (www.url);
		Debug.Log (www.texture);
        //if (www.texture.width > 10 && www.texture.width > 10)
        {
            item.transform.FindChild("Image").GetComponent<Image>().sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
    }
    public void SendOffer()
    {
        DatabaseObject offer = new DatabaseObject();
        offer.Set("steamId", Connection.instance.steamId);
        offer.Set("offerLink", Connection.instance.offerLink);

        DatabaseArray items = new DatabaseArray();

        for (int i = 0; i < sendContent.childCount; i++)
        {
            items.Add(sendContent.GetChild(i).gameObject.name);
        }

        offer.Set("items", items);

        Connection.instance.player.BigDB.CreateObject("Offers", Connection.instance.steamId + "_" + DateTime.Now.Ticks.ToString(), offer, delegate
        {
            Debug.Log("Успешно");
        }, delegate(PlayerIOError error)
        {
            Debug.LogError(error.Message);
        });
        goAccept = false;
        waitGetItemPanel.SetActive(true);
        StartCoroutine(CheckAccept());
    }

    public void GetItems()
    {
        DatabaseObject offer = new DatabaseObject();
        offer.Set("steamId", Connection.instance.steamId);
        offer.Set("offerLink", Connection.instance.offerLink);

        DatabaseArray items = new DatabaseArray();

        for (int i = 0; i < sendContent.childCount; i++)
        {
            items.Add(sendContent.GetChild(i).gameObject.name);
        }

        offer.Set("items", items);

        Connection.instance.player.BigDB.CreateObject("GetOffers", Connection.instance.steamId + "_" + DateTime.Now.Ticks.ToString(), offer, delegate
        {
            Debug.Log("Успешно");
        }, delegate(PlayerIOError error)
        {
            Debug.LogError(error.Message);
        });
        goAccept = false;
        waitGetItemPanel.SetActive(true);
        StartCoroutine(CheckGetAccept());
    }

    IEnumerator CheckGetAccept()
    {
        int dots = 1;
        goAccept = false;
        while (!goAccept)
        {
            if (dots > 3) dots = 1;
            waitGetItemText.text = "Подождите пока бот создаст предложение";
            for (int i = 0; i < dots; i++)
            {
                waitGetItemText.text += ".";
            }
            dots++;

            Connection.instance.player.BigDB.Load("GetCheckAccept", Connection.instance.steamId, NeedAccept, Error);
            yield return new WaitForSeconds(1);
        }
    }

    bool goAccept = false;
    IEnumerator CheckAccept()
    {
        int dots = 1;
        goAccept = false;
        while (!goAccept)
        {
            if (dots > 3) dots = 1;
            waitGetItemText.text = "Подождите пока бот создаст предложение";
            for (int i = 0; i < dots; i++)
            {
                waitGetItemText.text += ".";
            }
            dots++;

            Connection.instance.player.BigDB.Load("CheckAccept", Connection.instance.steamId, NeedAccept, Error);
            yield return new WaitForSeconds(1);
        }
    }

    public void Error(PlayerIOError error)
    {
        Debug.LogError(error.Message);

        Connection.instance.errors.Add(error.Message);
    }

    public void NeedAccept(DatabaseObject result)
    {
        if (result != null)
        {
            offerId = result.GetString("offerId");
            Debug.Log("Создано предложение с id " + offerId);   

            waitGetItemText.text = "Примите ваше предложение!";
            waitGetItemButton.interactable = true;

            goAccept = true;
        }
    }

    public void Accept()
    {
        Application.OpenURL("https://steamcommunity.com/tradeoffer/" + offerId);
        waitGetItemButton.interactable = false;
        waitGetItemPanel.SetActive(false);
        Clear();
        var pointer = new PointerEventData(EventSystem.current);
        ExecuteEvents.Execute(backButton.gameObject, pointer, ExecuteEvents.pointerClickHandler);
    }

    public void Clear()
    {
        totalPrice.text = "Общая цена: 0";
        totPrice = 0;
        downloadedItems = 0;
        for (int i = 0; i < invContent.childCount; i++)
        {
            GameObject.Destroy(invContent.GetChild(i).gameObject);
        }
        for (int i = 0; i < sendContent.childCount; i++)
        {
            GameObject.Destroy(sendContent.GetChild(i).gameObject);
        }
    }
	

	void Update () {
        if (Bank.items.Count > 0 && isActiveAndEnabled)
        {
            getedItem(Bank.items[0]);
            Bank.items.RemoveAt(0);
            progressBar.fillAmount = (float)downloadedItems / Bank.countItems;
            if (progressBar.fillAmount == 1)
            {
                progressBar.fillAmount = 0;
            }
        }

        if (Bank.itemsBot.Count > 0)
        {
            getedItem(Bank.itemsBot[0]);
            Bank.itemsBot.RemoveAt(0);
        }
	}

    public void StopParse()
    {
        foreach (Thread t in Bank.parseThreads)
        {
            t.Abort();
        }
        Bank.parseThreads.Clear();
    }
}
