﻿using UnityEngine;
using System.Collections;
using System.Threading;
using PlayerIOClient;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class MainMenuHandler : MonoBehaviour
{
    [Header("Choose screen")]
    public GameObject choosePanel;

    [Header("Login screen")]
    public InputField logLoginField;
    public InputField logPasswordField;
    public GameObject logPanel;

    [Header("Register screen")]
    public GameObject regPanel;
    public InputField regLoginField;
    public InputField regPassField;
    public InputField regPass2Field;
    public InputField regSteamIDField;
    public InputField regSteamTradeLinkField;

    [Header("Loged-in screen")]
    public GameObject logedInPanel;
    public Text nicknameText;
    public Text money;
    public Image avatar;
    public Text playersOnline;
    public Text botsOnline;

    [Header("InteractableButtons")]
    public Button addMoneyButton;
    public Button withdrawMoneyButton;
    public Button createRoomButton;

    [Header("Withdraw screen")]
    public Text playerMoneyText;

    [Header("Create room")]
    public InputField roomNameInputField;
    public InputField betValueInputField;
    public InputField bestOfInputField;

    public GameObject roomContent;
    public GameObject roomPrefab;

    [Header("Other")]
    public GameObject loading;

    const string ROCK_PAPER_SCISSORS = "Rock-paper-scissors";



    #region Add/Witdraw money screens
    [HideInInspector]
    public bool firstDownload = true;

    public void DownloadInventory()
    {
        if (firstDownload)
        {
            Thread downloadInventory = new Thread(new ParameterizedThreadStart(Bank.DownloadInventory));
            downloadInventory.Start(new BankInfo(Connection.instance.steamId, 730, 2));
            firstDownload = false;
        }
    }

    public void RefreshInventory(bool download)
    {
        firstDownload = true;
        DownloadInventory();
    }

    [HideInInspector]
    public bool firstDownloadItems = true;
    public void DownloadItems()
    {
        if (firstDownloadItems)
        {
            Bank.DownloadItemsBot();

            Connection.instance.player.BigDB.LoadMyPlayerObject(delegate(DatabaseObject result)
            {
                playerMoneyText.text = "Монет: " + result.GetInt("money").ToString();
                Connection.instance.money = result.GetInt("money");
            }, Connection.instance.Error);

            firstDownloadItems = false;
        }
    }

    public void RefreshItems()
    {
        firstDownloadItems = true;
        DownloadItems();
    }

    public void RefreshMoney()
    {
        Connection.instance.player.BigDB.LoadMyPlayerObject(delegate(DatabaseObject result)
        {
            Connection.instance.money = result.GetInt("money");
        }, Connection.instance.Error);
    }

    #endregion

    #region roomsCode
    public void CreateRoom()
    {
        if (roomNameInputField.text == "")
        {
            Connection.instance.errors.Add("Пожалуйста, введите имя комнаты");
            return;
        }
        if (betValueInputField.text == "")
        {
            Connection.instance.errors.Add("Пожалуйста, введите корректную ставку");
            return;
        }
        if (Convert.ToInt16(betValueInputField.text) < 2)
        {
            Connection.instance.errors.Add("Минимальная ставка - 2 монеты");
            return;
        }
        if (Convert.ToInt16(bestOfInputField.text) <= 0)
        {
            Connection.instance.errors.Add("Минимально значение поля \"Лучший с\" - 1");
            return;
        }
        if (Convert.ToInt16(betValueInputField.text) > Connection.instance.money)
        {
            Connection.instance.errors.Add("У вас недостаточно монет");
            return;
        }

        

        Dictionary<string, string> roomData = new Dictionary<string, string>(){
            {"name",roomNameInputField.text},
            {"bet",betValueInputField.text},
            {"creator",Connection.instance.user.personaname},
            {"maxPlayers","2"},
            {"bestOf", (bestOfInputField.text == "") ? "3" : bestOfInputField.text },
        };

        Dictionary<string, string> joinData = new Dictionary<string, string>(){
            {"name",Connection.instance.user.personaname},
            {"avatar",Connection.instance.user.avatarfull},
        };

        //TODO: do different type's
        Connection.instance.player.Multiplayer.CreateJoinRoom(roomNameInputField.text+"_"+Connection.instance.steamId, ROCK_PAPER_SCISSORS, true, roomData,
            joinData, Success,
            Connection.instance.Error);

        GameObject.Find("CreateRoomPanel").SetActive(false);
    }

    public void Success(PlayerIOClient.Connection connection)
    {
        Connection.instance.connection = connection;
        Application.LoadLevel(ROCK_PAPER_SCISSORS);
    }

    public void RefreshRooms()
    {
        Connection.instance.player.Multiplayer.ListRooms(ROCK_PAPER_SCISSORS, null, 0, 0, GetedRooms, Connection.instance.Error);
    }

    IEnumerator DisableButton(float time, Button but)
    {
        but.interactable = false;
        yield return new WaitForSeconds(time);
        but.interactable = true;
    }

    public void GetedRooms(RoomInfo[] rooms)
    {
        ClearRoomList();
        GameObject roomGO;
        foreach (RoomInfo room in rooms)
        {
            roomGO = Instantiate(roomPrefab) as GameObject;
            roomGO.name = room.Id;
            roomGO.transform.FindChild("Name").GetComponent<Text>().text = room.RoomData["name"] + " (лучший с "+room.RoomData["bestOf"]+")";
            roomGO.transform.FindChild("Bet").GetComponent<Text>().text = "Ставка: " + room.RoomData["bet"];
            if(room.RoomType == ROCK_PAPER_SCISSORS)
                roomGO.transform.FindChild("Type").GetComponent<Text>().text = @"<i>Камень-ножницы-бумага</i>";
            roomGO.transform.FindChild("CreatorName").GetComponent<Text>().text = "Создатель:" + room.RoomData["creator"];
            roomGO.GetComponent<Button>().onClick.AddListener(() =>
            {
                if (Connection.instance.money >= Convert.ToInt16(room.RoomData["bet"]))
                {
                    StartCoroutine(DisableButton(3f, roomGO.GetComponent<Button>()));
                    Dictionary<string, string> joinData = new Dictionary<string, string>(){
                    {"name",Connection.instance.user.personaname},
                    {"avatar",Connection.instance.user.avatarfull},
                    };
                    Connection.instance.player.Multiplayer.JoinRoom(room.Id, joinData, Success, Connection.instance.Error);
                }
                else
                {
                    Connection.instance.errors.Add("У вас недостаточно монет");
                }
            });
            roomGO.transform.SetParent(roomContent.transform, false);
        }
    }

    public void ClearRoomList(bool getNew = false)
    {
        for (int i = 0; i < roomContent.transform.childCount; i++)
        {
            Destroy(roomContent.transform.GetChild(i).gameObject);
        }
        if (getNew) RefreshRooms();
    }

    #endregion

    #region Connect/Register
    public void Connect()
    {
        if (logLoginField.text != "" && logPasswordField.text != "")
        {
            Connection.instance.state = Connection.State.Wait;
            Connection.instance.Connect(logLoginField.text, logPasswordField.text);
            logPanel.SetActive(false);
            loading.SetActive(true);
        }
        else
        {
            logPanel.SetActive(true);
            Connection.instance.errors.Add("Логин и/или пароль не могут быть пустыми");
        }
    }

    IEnumerator DownloadAvatar(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        avatar.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
    }

    public void Register()
    {
        if (regLoginField.text != "" && regPassField.text != "" && regPassField.text == regPass2Field.text && regSteamIDField.text != "" && regSteamTradeLinkField.text != "")
        {
            regPanel.SetActive(false);
            Connection.instance.state = Connection.State.Wait;
            Connection.instance.Register(regLoginField.text, regPassField.text, regSteamIDField.text, regSteamTradeLinkField.text);
        }
        else
        {
            Connection.instance.errors.Add("Заполните все поля");
            regPanel.SetActive(true);
        }
    }
    #endregion

    bool once = false;
    public void Update()
    {
        money.text ="Монет: "+ Connection.instance.money;
        if (!once && Connection.instance.user != null && Connection.instance.user.personaname != "")
        {
            once = true;
            InvokeRepeating("RefreshOnline", 0, 10f);
            nicknameText.text = Connection.instance.user.personaname;
            createRoomButton.interactable = true;
            if (Connection.instance.user.avatarfull != "")
                StartCoroutine(DownloadAvatar(Connection.instance.user.avatarfull));
            RefreshRooms();
            RefreshMoney();
            logedInPanel.SetActive(true);
            loading.SetActive(false);
        }

        if (Connection.instance.state == Connection.State.Menu)
        {
            if (logPanel.activeSelf == false && regPanel.activeSelf == false &&
                choosePanel.activeSelf == false)
            {
                logedInPanel.SetActive(false);
                choosePanel.SetActive(true);
                loading.SetActive(false);
            }
        }
    }

    public void RefreshOnline()
    {
        RefreshRooms();
        Connection.instance.player.PlayerInsight.Refresh(() =>
        {
            playersOnline.text = "Игроков в сети: " + Connection.instance.player.PlayerInsight.PlayersOnline.ToString();
        }, Connection.instance.Error);
        Connection.instance.player.Multiplayer.ListRooms("BotRoom", null, 1, 0, (RoomInfo[] rooms) =>
        {
            if (rooms.Length > 0)
            {
                botsOnline.text = "Ботов онлайн: " + rooms[0].RoomData["BotsCount"];
                playersOnline.text = "Игроков в сети: " + (Connection.instance.player.PlayerInsight.PlayersOnline - Int32.Parse(rooms[0].RoomData["BotsCount"])).ToString();
                addMoneyButton.interactable = true;
                withdrawMoneyButton.interactable = true;
            }
            if (rooms.Length == 0)
            {
                botsOnline.text = "Все боты не в сети";
                addMoneyButton.interactable = false;
                withdrawMoneyButton.interactable = false;
            }
        }, (PlayerIOError error) =>
        {
            botsOnline.text = "Все боты не в сети";
            addMoneyButton.interactable = false;
            withdrawMoneyButton.interactable = false;
        });
    }
}
