﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InputIntegerWithoutMinus : MonoBehaviour
{

    void Start()
    {
        InputField inputField = gameObject.GetComponent<InputField>();

        inputField.onValidateInput += ValidateInput;

    }

    public char ValidateInput(string text, int charIndex, char addedChar)
    {
        if (addedChar != '1'
           && addedChar != '2'
           && addedChar != '3'
           && addedChar != '4'
           && addedChar != '5'
           && addedChar != '6'
           && addedChar != '7'
           && addedChar != '8'
           && addedChar != '9'
           && addedChar != '0')
        {
            addedChar = '\0';
        }

        return addedChar;
    }
}
