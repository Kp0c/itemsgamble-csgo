﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ErrorManager : MonoBehaviour {

    public GameObject errorText;
    public Connection connection;

    public static ErrorManager instance;
	// Use this for initialization
	void Start () {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
        if (connection.errors.Count > 0)
        {
           GameObject temp = GameObject.Instantiate(errorText) as GameObject;
           temp.transform.SetParent(GameObject.Find("Canvas").transform,false);
           temp.GetComponent<Text>().text = connection.errors[0];
           connection.errors.RemoveAt(0);
           StartCoroutine(AutoDestructError(temp));
        }
	}

    IEnumerator AutoDestructError(GameObject error)
    {
        yield return new WaitForSeconds(5f);
        GameObject.Destroy(error);
    }
}
