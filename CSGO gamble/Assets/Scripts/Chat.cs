﻿using UnityEngine;
using System.Collections;
using PlayerIOClient;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Chat : MonoBehaviour {

    public Transform chatContent;
    public GameObject messagePrefab;
    public InputField chatInput;

    bool once = false;
    bool active = false;

	void Update () {
        if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.KeypadEnter) )
        {
            if (!active)
            {
                EventSystem.current.SetSelectedGameObject(chatInput.gameObject);
                active = true;
            }

            if (active && chatInput.text!="")
            {
                if (EventSystem.current.currentSelectedGameObject == chatInput.gameObject)
                {
                    Connection.instance.connection.Send("Chat", chatInput.text);
                    chatInput.text = "";
                    active = false;
                }
            }
        }

        if (!once && Connection.instance.connection != null)
        {
            Connection.instance.connection.AddOnMessage(OnMessage);
            once = true;
        }
        transform.SetAsLastSibling();
	}

    public void OnMessage(object sender, Message message)
    {
        switch (message.Type)
        {
            case "Chat":
                GameObject messageGO = Instantiate(messagePrefab) as GameObject;
                messageGO.GetComponent<Text>().text = message.GetString(0);
                messageGO.transform.SetParent(chatContent, false);
                break;
        }
    }
}
