﻿using System;
using UnityEditor;
using UnityEditor.Callbacks;
using Debug = UnityEngine.Debug;

public class WebBuiladAutoInc {

    static String[] levels = { "Assets/Scenes/MainMenu.unity", "Assets/Scenes/Rock-paper-scissors.unity" };

	static void BuildIncNumber() {
        if (EditorPrefs.HasKey("BUILD"))
        {
				string value = IncBuild(EditorPrefs.GetString("BUILD"));
                Debug.Log(string.Format("Build: {0} > {1}", EditorPrefs.GetString("BUILD"), value));
                EditorPrefs.SetString("BUILD", value);
        }
        else
        {
            EditorPrefs.SetString("BUILD", "0.0.0.1");
        }
	}

    [MenuItem ("BuildTools/Build/Standalone64/TestBuild")]
    static void TestBuild()
    {
        BuildOptions myBuildOptions = BuildOptions.Il2CPP | BuildOptions.Development | BuildOptions.AllowDebugging | BuildOptions.ShowBuiltPlayer;
		BuildPipeline.BuildPlayer( levels, @"C:\Users\Kp0c\Desktop\Game builded\"+EditorPrefs.GetString("BUILD","0.0.0.0").Replace(".","_")+@"\game.exe", 
					   BuildTarget.StandaloneWindows, myBuildOptions); 
	}

    [MenuItem("BuildTools/Build/Standalone64/Build")]
    static void Build()
    {
        BuildOptions myBuildOptions = BuildOptions.Il2CPP | BuildOptions.ShowBuiltPlayer;
        BuildPipeline.BuildPlayer(levels, @"C:\Users\Kp0c\Desktop\Game builded\" + EditorPrefs.GetString("BUILD", "0.0.0.0").Replace(".", "_") + @"\game.exe",
                       BuildTarget.StandaloneWindows, myBuildOptions);
    }

    #region increment
    [MenuItem("BuildTools/Increment/Major")]
    static void IncMajor()
    {
        string version;
        version =   EditorPrefs.GetString("BUILD", "0.0.0.0");

        string[] parts = version.Split('.');

        parts[0] = (Convert.ToInt32(parts[0])+1).ToString();
        parts[1] = "0";
        parts[2] = "0";

        EditorPrefs.SetString("BUILD", parts[0] + "." + parts[1] + "." + parts[2] + "." + parts[3]);
        Debug.Log("Build: "+EditorPrefs.GetString("BUILD"));
    }

    [MenuItem("BuildTools/Increment/Minor")]
    static void IncMinor()
    {
        string version;
        version = EditorPrefs.GetString("BUILD", "0.0.0.0");

        string[] parts = version.Split('.');

        parts[1] = (Convert.ToInt32(parts[1]) + 1).ToString();
        parts[2] = "0";

        EditorPrefs.SetString("BUILD", parts[0] + "." + parts[1] + "." + parts[2] + "." + parts[3]);
        Debug.Log("Build: "+EditorPrefs.GetString("BUILD"));
    }

    [MenuItem("BuildTools/Increment/Sub-Minor")]
    static void IncSubMinor()
    {
        string version;
        version = EditorPrefs.GetString("BUILD", "0.0.0.0");

        string[] parts = version.Split('.');

        parts[2] = (Convert.ToInt32(parts[2]) + 1).ToString();
        EditorPrefs.SetString("BUILD", parts[0] + "." + parts[1] + "." + parts[2] + "." + parts[3]);
        Debug.Log("Build: "+EditorPrefs.GetString("BUILD"));
    }
    #endregion

    [MenuItem("BuildTools/Increment/Refresh")]
    static void Refersh()
    {
        EditorPrefs.SetString("BUILD", "0.0.0.0");
    }

    [PostProcessBuild]
	private static void BuildIncNum(BuildTarget target, string pathToBuildProject) {
		EditorApplication.update += OnEditorUpdate;
	}
    static bool builded = false;
    [PostProcessSceneAttribute(0)]
    private static void BuildIncNum()
    {
        if (!builded)
        {
            EditorApplication.update += OnEditorUpdate;
            builded = true;
        }
    }

	public static void OnEditorUpdate() {
			EditorApplication.update -= OnEditorUpdate;
			BuildIncNumber();
	}

	private static string IncBuild(string source) {
		string[] parts = source.Split('.');
		string last = parts[parts.Length - 1];
		string result = "";
		int num;
		if (Int32.TryParse(last, out num)) {
			for (int i = 0; i < parts.Length - 1; i++) {
				result += parts[i] + ".";
			}
			return result + (num + 1);
		}
		return source;
	}
}
