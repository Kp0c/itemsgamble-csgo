using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace Patcher
{
	public class Main : Form
	{
		private Thread thread;

		private Thread thread1;

		private Thread thread2;

		private int totalPatches = 0;

		private string latestVersion = "???";

		private string masterUrl = "";

		private IContainer components = null;

		private ProgressBar progressBar1;

		private Button createPatch;

		private Label currentVersion;

		private GroupBox groupBox1;

		private Label totalPatchesLabel;

		private Label label2;

		private Label idLabel;

		private TextBox versionLabel;

		private TextBox oldPatchDirText;

		private Label label3;

		private Label label4;

		private TextBox newPatchDirText;

		private FolderBrowserDialog newFolder;

		private Label stats1Label;

		private Button oldButton;

		private Button newButton;

		private FolderBrowserDialog oldFolder;

		private CheckBox forceCheck;

		private GroupBox groupBox2;

		private Label label1;

		private RichTextBox richTextBox1;

		private GroupBox groupBox3;

		private GroupBox groupBox4;

		private Label label5;

		private Label label6;

		private TextBox mac_new;

		private TextBox mac_old;

		private Button button1;

		private Button button2;

		private GroupBox groupBox5;

		private Label label7;

		private Label label8;

		private TextBox linux_new;

		private TextBox linux_old;

		private Button button3;

		private Button button4;

		private FolderBrowserDialog macOldFolder;

		private FolderBrowserDialog macNewFolder;

		private FolderBrowserDialog linuxOldFolder;

		private FolderBrowserDialog linuxNewFolder;

		private BackgroundWorker backgroundWorker1;

		public Main()
		{
            InitializeComponent();
		}

		private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
		{
		}

		private void button1_Click(object sender, EventArgs e)
		{
            macNewFolder.ShowDialog();
            mac_new.Text = macNewFolder.SelectedPath;
		}

		private void button2_Click(object sender, EventArgs e)
		{
            macOldFolder.ShowDialog();
            mac_old.Text = macOldFolder.SelectedPath;
		}

		private void button3_Click(object sender, EventArgs e)
		{
            linuxNewFolder.ShowDialog();
            linux_new.Text = linuxNewFolder.SelectedPath;
		}

		private void button4_Click(object sender, EventArgs e)
		{
            linuxOldFolder.ShowDialog();
            linux_old.Text = linuxOldFolder.SelectedPath;
		}

		public void compareDirs(string oldPath, string newPath, string osString)
		{
			string[] strArrays;
			base.Invoke(new Action(() => stats1Label.Text = string.Concat("Comparing files for ", osString, "...")));
			string[] files1 = Directory.GetFiles(oldPath, "*.*", SearchOption.AllDirectories);
			int dir1Size = (int)files1.Length;
			Console.WriteLine(string.Concat("dir1Size: ", dir1Size));
			string[] files2 = Directory.GetFiles(newPath, "*.*", SearchOption.AllDirectories);
			int length = (int)files2.Length;
			base.Invoke(new Action(() => progressBar1.Maximum = length));
			Console.WriteLine(string.Concat("dir2Size: ", length));
			int num = 0;
			string[] strArrays1 = files2;
			for (int i = 0; i < (int)strArrays1.Length; i++)
			{
				string f2 = strArrays1[i];
				bool exists = false;
				string[] strArrays2 = files1;
				for (int j = 0; j < (int)strArrays2.Length; j++)
				{
					string f1 = strArrays2[j];
					object[] objArray = new object[] { "(", num, " / ", length, " of ", osString, ") Comparing new file: ", Path.GetFileName(f2), " to old: ", Path.GetFileName(f1) };
					Console.WriteLine(string.Concat(objArray));
					if (Main.FileEquals(f2, f1))
					{
						if (f2.Replace(newPath, "").Equals(f1.Replace(oldPath, "")))
						{
							exists = true;
							break;
						}
					}
				}
				if (!exists)
				{
					string fileName = Path.GetFileName(f2);
					string patch = f2.Replace(newPath, "");
					patch = patch.Replace(fileName, "");
					strArrays = new string[] { "patch/", osString, "_update_", versionLabel.Text.Replace(".", "_").Replace(",","_"), "/", patch };
					Directory.CreateDirectory(string.Concat(strArrays));
					strArrays = new string[] { "patch/", osString, "_update_", versionLabel.Text.Replace(".", "_").Replace(",", "_"), "/", patch, "/", fileName };
					File.Copy(f2, string.Concat(strArrays), true);
				}
				num++;
				base.Invoke(new Action(() => progressBar1.Value = num));
			}
			base.Invoke(new Action(() => stats1Label.Text = string.Concat("Zip the patch for ", osString, "...")));
			strArrays = new string[] { "patch/", osString, "_update_", versionLabel.Text.Replace(".", "_").Replace(",","_"), ".zip" };
            CreateSample(string.Concat(strArrays), null, string.Concat("patch/", osString, "_update_", versionLabel.Text.Replace(".", "_").Replace(",","_")), osString);
			strArrays = new string[] { "patch/", osString, "_update_", versionLabel.Text.Replace(".", "_").Replace(",","_"), "/" };
			Directory.Delete(string.Concat(strArrays), true);
			if (osString.Equals("win"))
			{
				if (!(mac_new.Text.Equals("") ? true : mac_old.Text.Equals("")))
				{
                    createMac();
				}
				else if ((linux_new.Text.Equals("") ? true : linux_old.Text.Equals("")))
				{
                    finishPatch();
				}
				else
				{
                    createLinux();
				}
			}
			else if (!osString.Equals("mac"))
			{
                finishPatch();
			}
			else if ((linux_new.Text.Equals("") ? true : linux_old.Text.Equals("")))
			{
                finishPatch();
			}
			else
			{
                createLinux();
			}
		}

		private void CompressFolder(string path, ZipOutputStream zipStream, int folderOffset)
		{
			int i;
			string[] files = Directory.GetFiles(path);
			for (i = 0; i < (int)files.Length; i++)
			{
				string filename = files[i];
				FileInfo fi = new FileInfo(filename);
				string entryName = filename.Substring(folderOffset);
				ZipEntry newEntry = new ZipEntry(ZipEntry.CleanName(entryName));
				//newEntry.set_DateTime(fi.LastWriteTime);
                newEntry.DateTime = fi.LastWriteTime;
				//newEntry.set_Size(fi.Length);
                newEntry.Size = fi.Length;
				zipStream.PutNextEntry(newEntry);
				byte[] buffer = new byte[4096];
				FileStream streamReader = File.OpenRead(filename);
				try
				{
					StreamUtils.Copy(streamReader, zipStream, buffer);
				}
				finally
				{
					if (streamReader != null)
					{
						((IDisposable)streamReader).Dispose();
					}
				}
				zipStream.CloseEntry();
			}
			files = Directory.GetDirectories(path);
			for (i = 0; i < (int)files.Length; i++)
			{
                CompressFolder(files[i], zipStream, folderOffset);
			}
		}

		private static int CountChars(string value)
		{
			int result = 0;
			bool lastWasSpace = false;
			string str = value;
			for (int i = 0; i < str.Length; i++)
			{
				if (!char.IsWhiteSpace(str[i]))
				{
					result++;
					lastWasSpace = false;
				}
				else
				{
					if (!lastWasSpace)
					{
						result++;
					}
					lastWasSpace = true;
				}
			}
			return result;
		}

		private void createLinux()
		{
            thread2 = new Thread(() => compareDirs(linux_old.Text, linux_new.Text, "linux"));
            thread2.Start();
		}

		private void createMac()
		{
            thread1 = new Thread(() => compareDirs(mac_old.Text, mac_new.Text, "mac"));
            thread1.Start();
		}

		private void createPatch_Click(object sender, EventArgs e)
		{
			if (versionLabel.Text.Equals(""))
			{
				MessageBox.Show("Please enter a valid version", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
			}
			else if (oldPatchDirText.Text.Equals(""))
			{
				MessageBox.Show("Please enter a valid path to the current windows version", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
			}
			else if (!newPatchDirText.Text.Equals(""))
			{
                createPatches();
			}
			else
			{
				MessageBox.Show("Please enter a valid path to the new windows version", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
			}
		}

		private void createPatches()
		{
            thread = new Thread(() => compareDirs(oldPatchDirText.Text, newPatchDirText.Text, "win"));
            thread.Start();
		}

		public void CreateSample(string outPathname, string password, string folderName, string osVersion)
		{
			ZipOutputStream zipStream = new ZipOutputStream(File.Create(outPathname));
			zipStream.SetLevel(3);
			//zipStream.set_Password(password);
            zipStream.Password = password;
			string[] strArrays = new string[] { "patch/", osVersion, "_update_", versionLabel.Text.Replace(".", "_").Replace(",","_"), "/" };
			int folderOffset = Main.CountChars(string.Concat(strArrays));
            CompressFolder(folderName, zipStream, folderOffset);
			//zipStream.set_IsStreamOwner(true);
            zipStream.IsStreamOwner = true;
			zipStream.Close();
		}

		protected override void Dispose(bool disposing)
		{
			if ((!disposing ? false : components != null))
			{
                components.Dispose();
			}
			base.Dispose(disposing);
		}

		private static bool FileEquals(string path1, string path2)
		{
			bool flag;
			byte[] file1 = File.ReadAllBytes(path1);
			byte[] file2 = File.ReadAllBytes(path2);
			if ((int)file1.Length != (int)file2.Length)
			{
				flag = false;
			}
			else
			{
				int i = 0;
				while (i < (int)file1.Length)
				{
					if (file1[i] == file2[i])
					{
						i++;
					}
					else
					{
						flag = false;
						return flag;
					}
				}
				flag = true;
			}
			return flag;
		}

		private void finishPatch()
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(File.ReadAllText("./patch/versionInfo.xml"));
			XmlElement patchElement = doc.CreateElement("patch");
			XmlElement idElement = doc.CreateElement("id");
			XmlElement buildElement = doc.CreateElement("build");
			XmlElement linkElement = doc.CreateElement("win-link");
			XmlElement maclinkElement = doc.CreateElement("mac-link");
			XmlElement linuxlinkElement = doc.CreateElement("linux-link");
			XmlElement forceElement = doc.CreateElement("force");
			XmlElement xmlElement = doc.CreateElement("notes");
			idElement.InnerText = string.Concat(totalPatches + 1);
			buildElement.InnerText = versionLabel.Text.Replace(",", ".");
			linkElement.InnerText = string.Concat("win_update_", versionLabel.Text.Replace(".", "_").Replace(",","_"), ".zip");
			if ((mac_new.Text.Equals("") ? true : mac_old.Text.Equals("")))
			{
				maclinkElement.InnerText = "none";
			}
			else
			{
				maclinkElement.InnerText = string.Concat("mac_update_", versionLabel.Text.Replace(".", "_").Replace(",","_"), ".zip");
			}
			if ((linux_new.Text.Equals("") ? true : linux_old.Text.Equals("")))
			{
				linuxlinkElement.InnerText = "none";
			}
			else
			{
				linuxlinkElement.InnerText = string.Concat("linux_update_", versionLabel.Text.Replace(".", "_").Replace(",","_"), ".zip");
			}
			base.Invoke(new Action(() => xmlElement.InnerText = WebUtility.HtmlDecode(richTextBox1.Text)));
			if (!forceCheck.Checked)
			{
				forceElement.InnerText = "false";
			}
			else
			{
				forceElement.InnerText = "true";
			}
			patchElement.AppendChild(idElement);
			patchElement.AppendChild(buildElement);
			patchElement.AppendChild(linkElement);
			patchElement.AppendChild(forceElement);
			patchElement.AppendChild(maclinkElement);
			patchElement.AppendChild(linuxlinkElement);
			patchElement.AppendChild(xmlElement);
			doc.DocumentElement.AppendChild(patchElement);
			doc.Save("./patch/versionInfo.xml");
			Console.WriteLine(string.Concat("Opening: ", AppDomain.CurrentDomain.BaseDirectory, "\\patch\\"));
			MessageBox.Show("Patch complete, now update the files and its online!", "COMPLETE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
			Process.Start(string.Concat(AppDomain.CurrentDomain.BaseDirectory, "\\patch\\"));
			Application.Exit();
		}

		private void getPatchInformation()
		{
            stats1Label.Text = "Getting latest version info...";
			string htmlCode = "NO-VERSION";
			try
			{
				WebClient client = new WebClient();
				htmlCode = client.DownloadString(string.Concat(masterUrl, "versionInfo.xml"));
			}
			catch (WebException webException)
			{
				Console.WriteLine(string.Concat("[SYSTEM] There was an error connecting: ", webException.Message));
				MessageBox.Show("Could not contact the update server. Please try again later.", "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
				Application.Exit();
			}
			XmlDocument xml = new XmlDocument();
			try
			{
				xml.LoadXml(htmlCode);
			}
			catch (Exception exception)
			{
				Console.Write(string.Concat("[ERROR] ", exception.Message));
			}
			XmlNodeList xnList = xml.SelectNodes("/versions/patch");
			Console.WriteLine(string.Concat("TOTAL PATCHES: ", xnList.Count));
            totalPatches = xnList.Count;
			Label label = idLabel;
			label.Text = string.Concat(label.Text, xnList.Count + 1);
			foreach (XmlNode xn in xnList)
			{
				if (xn["id"].InnerText.Equals(string.Concat(totalPatches)))
				{
					Console.WriteLine(string.Concat("Latest patch version: ", xn["build"].InnerText));
                    latestVersion = xn["build"].InnerText;
				}
			}
			Label label1 = currentVersion;
			label1.Text = string.Concat(label1.Text, latestVersion);
			Label label2 = totalPatchesLabel;
			label2.Text = string.Concat(label2.Text, totalPatches);
			Directory.CreateDirectory("patch/");
			StreamWriter writer = new StreamWriter("./patch/versionInfo.xml", false);
			try
			{
				writer.WriteLine(htmlCode);
			}
			finally
			{
				if (writer != null)
				{
					((IDisposable)writer).Dispose();
				}
			}
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            progressBar1 = new System.Windows.Forms.ProgressBar();
            createPatch = new Button();
            currentVersion = new System.Windows.Forms.Label();
            groupBox1 = new System.Windows.Forms.GroupBox();
            totalPatchesLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            idLabel = new System.Windows.Forms.Label();
            versionLabel = new System.Windows.Forms.TextBox();
            oldPatchDirText = new System.Windows.Forms.TextBox();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            newPatchDirText = new System.Windows.Forms.TextBox();
            newFolder = new System.Windows.Forms.FolderBrowserDialog();
            stats1Label = new System.Windows.Forms.Label();
            oldButton = new Button();
            newButton = new Button();
            oldFolder = new System.Windows.Forms.FolderBrowserDialog();
            forceCheck = new CheckBox();
            groupBox2 = new System.Windows.Forms.GroupBox();
            groupBox5 = new System.Windows.Forms.GroupBox();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            linux_new = new System.Windows.Forms.TextBox();
            linux_old = new System.Windows.Forms.TextBox();
            button3 = new Button();
            button4 = new Button();
            label1 = new System.Windows.Forms.Label();
            richTextBox1 = new System.Windows.Forms.RichTextBox();
            groupBox3 = new System.Windows.Forms.GroupBox();
            groupBox4 = new System.Windows.Forms.GroupBox();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            mac_new = new System.Windows.Forms.TextBox();
            mac_old = new System.Windows.Forms.TextBox();
            button1 = new Button();
            button2 = new Button();
            macOldFolder = new System.Windows.Forms.FolderBrowserDialog();
            macNewFolder = new System.Windows.Forms.FolderBrowserDialog();
            linuxOldFolder = new System.Windows.Forms.FolderBrowserDialog();
            linuxNewFolder = new System.Windows.Forms.FolderBrowserDialog();
            backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox5.SuspendLayout();
            groupBox3.SuspendLayout();
            groupBox4.SuspendLayout();
            SuspendLayout();
            // 
            // progressBar1
            // 
            progressBar1.Location = new System.Drawing.Point(14, 404);
            progressBar1.Name = "progressBar1";
            progressBar1.Size = new System.Drawing.Size(679, 23);
            progressBar1.TabIndex = 0;
            // 
            // createPatch
            // 
            createPatch.Location = new System.Drawing.Point(14, 375);
            createPatch.Name = "createPatch";
            createPatch.Size = new System.Drawing.Size(679, 23);
            createPatch.TabIndex = 1;
            createPatch.Text = "Create Patch";
            createPatch.UseVisualStyleBackColor = true;
            createPatch.Click += new System.EventHandler(createPatch_Click);
            // 
            // currentVersion
            // 
            currentVersion.AutoSize = true;
            currentVersion.Location = new System.Drawing.Point(6, 16);
            currentVersion.Name = "currentVersion";
            currentVersion.Size = new System.Drawing.Size(118, 13);
            currentVersion.TabIndex = 2;
            currentVersion.Text = "Current (latest) version: ";
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(totalPatchesLabel);
            groupBox1.Controls.Add(currentVersion);
            groupBox1.Location = new System.Drawing.Point(13, 12);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new System.Drawing.Size(331, 53);
            groupBox1.TabIndex = 3;
            groupBox1.TabStop = false;
            groupBox1.Text = "Information";
            // 
            // totalPatchesLabel
            // 
            totalPatchesLabel.AutoSize = true;
            totalPatchesLabel.Location = new System.Drawing.Point(6, 33);
            totalPatchesLabel.Name = "totalPatchesLabel";
            totalPatchesLabel.Size = new System.Drawing.Size(128, 13);
            totalPatchesLabel.TabIndex = 3;
            totalPatchesLabel.Text = "Total number of patches: ";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(6, 37);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(99, 13);
            label2.TabIndex = 4;
            label2.Text = "New patch version:";
            // 
            // idLabel
            // 
            idLabel.AutoSize = true;
            idLabel.Location = new System.Drawing.Point(6, 16);
            idLabel.Name = "idLabel";
            idLabel.Size = new System.Drawing.Size(76, 13);
            idLabel.TabIndex = 5;
            idLabel.Text = "New patch id: ";
            // 
            // versionLabel
            // 
            versionLabel.Location = new System.Drawing.Point(111, 34);
            versionLabel.Name = "versionLabel";
            versionLabel.Size = new System.Drawing.Size(213, 20);
            versionLabel.TabIndex = 6;
            // 
            // oldPatchDirText
            // 
            oldPatchDirText.Location = new System.Drawing.Point(111, 13);
            oldPatchDirText.Name = "oldPatchDirText";
            oldPatchDirText.Size = new System.Drawing.Size(132, 20);
            oldPatchDirText.TabIndex = 7;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(6, 16);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(88, 13);
            label3.TabIndex = 8;
            label3.Text = "Current patch dir:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(6, 42);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(76, 13);
            label4.TabIndex = 10;
            label4.Text = "New patch dir:";
            // 
            // newPatchDirText
            // 
            newPatchDirText.Location = new System.Drawing.Point(111, 39);
            newPatchDirText.Name = "newPatchDirText";
            newPatchDirText.Size = new System.Drawing.Size(132, 20);
            newPatchDirText.TabIndex = 9;
            // 
            // newFolder
            // 
            newFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // stats1Label
            // 
            stats1Label.AutoSize = true;
            stats1Label.Location = new System.Drawing.Point(12, 433);
            stats1Label.Name = "stats1Label";
            stats1Label.Size = new System.Drawing.Size(81, 13);
            stats1Label.TabIndex = 11;
            stats1Label.Text = "No info to show";
            stats1Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // oldButton
            // 
            oldButton.Location = new System.Drawing.Point(249, 13);
            oldButton.Name = "oldButton";
            oldButton.Size = new System.Drawing.Size(86, 20);
            oldButton.TabIndex = 12;
            oldButton.Text = "Find";
            oldButton.UseVisualStyleBackColor = true;
            oldButton.Click += new System.EventHandler(oldButton_Click);
            // 
            // newButton
            // 
            newButton.Location = new System.Drawing.Point(249, 39);
            newButton.Name = "newButton";
            newButton.Size = new System.Drawing.Size(86, 20);
            newButton.TabIndex = 13;
            newButton.Text = "Find";
            newButton.UseVisualStyleBackColor = true;
            newButton.Click += new System.EventHandler(newButton_Click);
            // 
            // oldFolder
            // 
            oldFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // forceCheck
            // 
            forceCheck.AutoSize = true;
            forceCheck.Location = new System.Drawing.Point(10, 275);
            forceCheck.Name = "forceCheck";
            forceCheck.Size = new System.Drawing.Size(83, 17);
            forceCheck.TabIndex = 14;
            forceCheck.Text = "Force patch";
            forceCheck.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(groupBox5);
            groupBox2.Controls.Add(label1);
            groupBox2.Controls.Add(richTextBox1);
            groupBox2.Controls.Add(idLabel);
            groupBox2.Controls.Add(forceCheck);
            groupBox2.Controls.Add(label2);
            groupBox2.Controls.Add(versionLabel);
            groupBox2.Location = new System.Drawing.Point(14, 77);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new System.Drawing.Size(679, 292);
            groupBox2.TabIndex = 15;
            groupBox2.TabStop = false;
            groupBox2.Text = "New patch information";
            // 
            // groupBox5
            // 
            groupBox5.Controls.Add(label7);
            groupBox5.Controls.Add(label8);
            groupBox5.Controls.Add(linux_new);
            groupBox5.Controls.Add(linux_old);
            groupBox5.Controls.Add(button3);
            groupBox5.Controls.Add(button4);
            groupBox5.Enabled = false;
            groupBox5.Location = new System.Drawing.Point(647, 4);
            groupBox5.Name = "groupBox5";
            groupBox5.Size = new System.Drawing.Size(341, 75);
            groupBox5.TabIndex = 17;
            groupBox5.TabStop = false;
            groupBox5.Text = "Linux";
            groupBox5.Visible = false;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(6, 16);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(88, 13);
            label7.TabIndex = 8;
            label7.Text = "Current patch dir:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(6, 42);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(76, 13);
            label8.TabIndex = 10;
            label8.Text = "New patch dir:";
            // 
            // linux_new
            // 
            linux_new.Location = new System.Drawing.Point(111, 39);
            linux_new.Name = "linux_new";
            linux_new.Size = new System.Drawing.Size(132, 20);
            linux_new.TabIndex = 9;
            // 
            // linux_old
            // 
            linux_old.Location = new System.Drawing.Point(111, 13);
            linux_old.Name = "linux_old";
            linux_old.Size = new System.Drawing.Size(132, 20);
            linux_old.TabIndex = 7;
            // 
            // button3
            // 
            button3.Location = new System.Drawing.Point(249, 39);
            button3.Name = "button3";
            button3.Size = new System.Drawing.Size(86, 20);
            button3.TabIndex = 13;
            button3.Text = "Find";
            button3.UseVisualStyleBackColor = true;
            button3.Click += new System.EventHandler(button3_Click);
            // 
            // button4
            // 
            button4.Location = new System.Drawing.Point(249, 13);
            button4.Name = "button4";
            button4.Size = new System.Drawing.Size(86, 20);
            button4.TabIndex = 12;
            button4.Text = "Find";
            button4.UseVisualStyleBackColor = true;
            button4.Click += new System.EventHandler(button4_Click);
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(6, 60);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(95, 13);
            label1.TabIndex = 16;
            label1.Text = "Patch notes (html):";
            // 
            // richTextBox1
            // 
            richTextBox1.DetectUrls = false;
            richTextBox1.Location = new System.Drawing.Point(10, 76);
            richTextBox1.Name = "richTextBox1";
            richTextBox1.Size = new System.Drawing.Size(663, 193);
            richTextBox1.TabIndex = 15;
            richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(label3);
            groupBox3.Controls.Add(groupBox4);
            groupBox3.Controls.Add(label4);
            groupBox3.Controls.Add(newPatchDirText);
            groupBox3.Controls.Add(oldPatchDirText);
            groupBox3.Controls.Add(newButton);
            groupBox3.Controls.Add(oldButton);
            groupBox3.Location = new System.Drawing.Point(352, 12);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new System.Drawing.Size(341, 63);
            groupBox3.TabIndex = 16;
            groupBox3.TabStop = false;
            groupBox3.Text = "Windows";
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(label5);
            groupBox4.Controls.Add(label6);
            groupBox4.Controls.Add(mac_new);
            groupBox4.Controls.Add(mac_old);
            groupBox4.Controls.Add(button1);
            groupBox4.Controls.Add(button2);
            groupBox4.Enabled = false;
            groupBox4.Location = new System.Drawing.Point(309, 0);
            groupBox4.Name = "groupBox4";
            groupBox4.Size = new System.Drawing.Size(341, 70);
            groupBox4.TabIndex = 17;
            groupBox4.TabStop = false;
            groupBox4.Text = "Mac";
            groupBox4.Visible = false;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(6, 16);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(88, 13);
            label5.TabIndex = 8;
            label5.Text = "Current patch dir:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(6, 42);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(76, 13);
            label6.TabIndex = 10;
            label6.Text = "New patch dir:";
            // 
            // mac_new
            // 
            mac_new.Location = new System.Drawing.Point(111, 39);
            mac_new.Name = "mac_new";
            mac_new.Size = new System.Drawing.Size(132, 20);
            mac_new.TabIndex = 9;
            // 
            // mac_old
            // 
            mac_old.Location = new System.Drawing.Point(111, 13);
            mac_old.Name = "mac_old";
            mac_old.Size = new System.Drawing.Size(132, 20);
            mac_old.TabIndex = 7;
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(249, 39);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(86, 20);
            button1.TabIndex = 13;
            button1.Text = "Find";
            button1.UseVisualStyleBackColor = true;
            button1.Click += new System.EventHandler(button1_Click);
            // 
            // button2
            // 
            button2.Location = new System.Drawing.Point(249, 13);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(86, 20);
            button2.TabIndex = 12;
            button2.Text = "Find";
            button2.UseVisualStyleBackColor = true;
            button2.Click += new System.EventHandler(button2_Click);
            // 
            // macOldFolder
            // 
            macOldFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // macNewFolder
            // 
            macNewFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // linuxOldFolder
            // 
            linuxOldFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // linuxNewFolder
            // 
            linuxNewFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // backgroundWorker1
            // 
            backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorker1_DoWork);
            // 
            // Main
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(705, 452);
            Controls.Add(groupBox3);
            Controls.Add(groupBox2);
            Controls.Add(stats1Label);
            Controls.Add(groupBox1);
            Controls.Add(createPatch);
            Controls.Add(progressBar1);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            MaximumSize = new System.Drawing.Size(3700, 3310);
            MinimumSize = new System.Drawing.Size(370, 331);
            Name = "Main";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "GamePatcher - FREE";
            Load += new System.EventHandler(Main_Load);
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox5.ResumeLayout(false);
            groupBox5.PerformLayout();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            groupBox4.ResumeLayout(false);
            groupBox4.PerformLayout();
            ResumeLayout(false);
            PerformLayout();

		}

		private void Main_Load(object sender, EventArgs e)
		{
            readUrl();
            getPatchInformation();
		}

		private void newButton_Click(object sender, EventArgs e)
		{
            newFolder.ShowDialog();
            newPatchDirText.Text = newFolder.SelectedPath;
		}

		private void oldButton_Click(object sender, EventArgs e)
		{
            oldFolder.ShowDialog();
            oldPatchDirText.Text = oldFolder.SelectedPath;
		}

		public void readUrl()
		{
			XmlDocument xml = null;
			try
			{
				xml = new XmlDocument();
				xml.Load("./conf/patcher.xml");
				foreach (XmlNode xn in xml.SelectNodes("/conf"))
				{
                    masterUrl = xn["url"].InnerText;
				}
			}
			catch (Exception exception)
			{
				Exception e3 = exception;
				Console.WriteLine(string.Concat("[SYSTEM] No configuration file found. ", e3));
				MessageBox.Show(string.Concat("No configuration file found. ", e3), "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
				Application.Exit();
			}
		}
    }
}