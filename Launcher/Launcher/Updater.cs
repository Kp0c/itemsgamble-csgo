﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Launcher
{
    static class Updater
    {
        public static int version = 9;
        public static string url = @"http://tdb.host22.com/update_launcher/";

        static bool updateLauncher = false;
        static bool updateDLL = false;
        static bool updateTool = false;
        static public void CheckUpdate()
        {
            if (!File.Exists("ICSharpCode.SharpZipLib.dll"))
                updateDLL = true;
            if (!File.Exists("UpdateTool.exe"))
                updateTool = true;

            checkVersionXML();
        }

        private static void Update()
        {
            UpdaterForm updater = new UpdaterForm();
            updater.updateDLL = updateDLL;
            updater.updateLauncher = updateLauncher;
            updater.updateTool = updateTool;
            updater.ShowDialog();
        }

        private static void checkVersionXML()
        {
            string htmlCode = "NO-VERSION";
            try
            {
                WebClient client = new WebClient();
                htmlCode = client.DownloadString(string.Concat(url, "version.xml"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не могу подключиться к серверу обновлений. ERROR: " + ex.Message);
            }

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(htmlCode);
            XmlNodeList xnList = xml.SelectNodes("/version");
            foreach (XmlNode xn in xnList)
            {
                if (version < Convert.ToInt32(xn.InnerText))
                {
                    updateLauncher = true;
                }
            }

            Update();
        }
    }
}
