﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Launcher.Properties;
using System.IO;
using System.Xml;

namespace Launcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Console.WriteLine("[SYSTEM] Лаунчер запущен");
            log("[SYSTEM] Лаунчер запущен");
            Updater.CheckUpdate();
        }

        private Thread thread;

        private string version = null;

        private string newVersionNumber;

        private string newVersionFile;

        private string url = "";

        private string name = "";

        private string toolTipString = "";

        private int currentIt = 0;

        private int totalIt = 0;

        private WebClient webClient;

        private Stopwatch sw = new Stopwatch();

        private IContainer components = null;
        private Dictionary<string, string> patches = new Dictionary<string, string>();

        public void check()
        {
            if (checkVersionXML())
            {
                string newVersionTemp = newVersionNumber.Replace(".", "_");
                DownloadFile(string.Concat(url, "files/", newVersionFile), string.Concat(Directory.GetCurrentDirectory(), "/temp/win_update_",
                    newVersionTemp, ".zip"));
            }
            else
            {
                Dispatcher.Invoke(new Action(() =>
                {
                    playButton.IsEnabled = true;
                    repairButton.IsEnabled = true;
                }));
            }
        }

        private bool checkVersionXML()
        {
            Exception e;
            string id;
            string versionName;
            bool flag;
            string htmlCode = "NO-VERSION";
            try
            {
                WebClient client = new WebClient();
                htmlCode = client.DownloadString(string.Concat(url, "versionInfo.xml"));
            }
            catch (Exception exception)
            {
                e = exception;
                Console.WriteLine(string.Concat("[SYSTEM] Не могу присоединиться к серверу обновлений. ERROR: ", e.Message));
                log(string.Concat("[SYSTEM] Не могу присоединиться к серверу обновлений. ERROR: ", e.Message));
                MessageBox.Show("Не могу присоединиться к серверу обновлений. Пожалуйста, попробуйте позже.", "Ошибка обновления");
                Application.Current.Shutdown();
            }
            if (htmlCode.Equals("NO-VERSION"))
            {
                Console.WriteLine("[SYSTEM] Произошла ошибка при получении последей версии: NO-VERSION");
                log("[SYSTEM] Произошла ошибка при получении последей версии: NO-VERSION");
                MessageBox.Show("Произошла ошибка при получении последей версии. Пожалуйста, попробуйте позже.", "Ошибка обновления");
                Application.Current.Shutdown();
            }
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(htmlCode);
            string thisId = null;
            string str = null;
            XmlNodeList xnList = xml.SelectNodes("/versions/patch");

            int idMax = 0;
            string note = "NO-NOTE";
            foreach (XmlNode xn in xnList)
            {
                id = xn["id"].InnerText;
                versionName = xn["build"].InnerText;
                if (versionName.Equals(version))
                {
                    thisId = id;
                    str = versionName;
                    try
                    {
                        
                        if (notes.Text != "")
                            break;
                    }
                    catch (Exception) { }
                }
                if (Convert.ToInt32(id) > idMax)
                {
                    idMax = Convert.ToInt32(id);
                    if (xn["notes"] != null)
                    {
                        note = xn["notes"].InnerText;
                        Dispatcher.Invoke(new Action(() =>
                        {
                            if (!patches.ContainsKey(xn["build"].InnerText))
                            {
                                patchesComboBox.Items.Add(xn["build"].InnerText);
                                patches.Add(xn["build"].InnerText, xn["notes"].InnerText);
                            }
                        }));

                    }
                }
            }



            Dispatcher.Invoke(new Action(() =>
            {
                try
                {
                    //webBrowser1.NavigateToString(note);
                    notes.Text = note;
                    patchesComboBox.Text = patchesComboBox.Items[patchesComboBox.Items.Count - 1].ToString();
                }
                catch (Exception) { }
            }));

            try
            {
                if (thisId.Equals(null))
                {
                    Console.WriteLine("[SYSTEM] Не могу найти версию");
                    log("[SYSTEM] Не могу найти версию");
                    MessageBox.Show("Ошибка с установкой. Пожалуйста, переустановите апдейтер (если у вас эта ошибка - отпишите создателю (Kp0c)).", "Ошибка обновления");
                    Application.Current.Shutdown();
                }
            }
            catch (Exception exception1)
            {
                e = exception1;
                Console.WriteLine(string.Concat("[SYSTEM] ERROR: ", e.Message));
                log(string.Concat("[SYSTEM] ERROR: ", e.Message));
            }
            if (totalIt == 0)
            {
                foreach (XmlNode xmlNodes in xnList)
                {
                    if (Convert.ToInt32(thisId) < Convert.ToInt32(xmlNodes["id"].InnerText))
                    {
                        MainWindow main = this;
                        main.totalIt = main.totalIt + 1;
                    }
                }
            }
            foreach (XmlNode xmlNodes1 in xnList)
            {
                id = xmlNodes1["id"].InnerText;
                versionName = xmlNodes1["build"].InnerText;
                if (id.Equals(string.Concat(Convert.ToInt32(thisId) + 1)))
                {
                    string[] strArrays = new string[] { "[SYSTEM] Клиент нуждается в обновлении с: ", version, " (#", thisId, ") до: ", versionName, " (#", id, ")" };
                    Console.WriteLine(string.Concat(strArrays));
                    strArrays = new string[] { "[SYSTEM] Клиент нуждается в обновлении с: ", version, " (#", thisId, ") до: ", versionName, " (#", id, ")" };
                    log(string.Concat(strArrays));
                    newVersionNumber = versionName;
                    newVersionFile = xmlNodes1["win-link"].InnerText;
                    flag = true;
                    return flag;
                }
            }
            Dispatcher.Invoke(new Action(() =>
            {
                progressBar.Maximum = 100;
                progressBar.Value = 100;
                textField.Content = string.Concat("v.", str);
            }));

            Console.WriteLine("[SYSTEM] Клиент актуален.");
            log("[SYSTEM] Клиент актуален.");
            flag = false;
            return flag;
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            Console.WriteLine("[SYSTEM] Загрузка завершена");
            log("[SYSTEM] Загрузка завершена");
            sw.Reset();
            if (!e.Cancelled)
            {
               Dispatcher.Invoke(new Action(()=> progressBar.Value = 0));
                Console.WriteLine("[SYSTEM] Установка обновления...");
                log("[SYSTEM] Установка обновления...");
                string newVersionTemp = newVersionNumber.Replace(".", "_");
                if (!Directory.Exists(string.Concat(Directory.GetCurrentDirectory(), "/game/")))
                {
                    Directory.CreateDirectory(string.Concat(Directory.GetCurrentDirectory(), "/game/"));
                }
                ExtractZipFile(string.Concat(Directory.GetCurrentDirectory(), "/temp/win_update_", newVersionTemp, ".zip"), null,
                    string.Concat(Directory.GetCurrentDirectory(), "/game/"));
            }
            else
            {
                Console.WriteLine("[SYSTEM] Загрузка была отменена.");
                log("[SYSTEM] Загрузка была отменена.");
                MessageBox.Show("[SYSTEM] Не могу присоединиться к серверу обновлений. Пожалуйста, попробуйте позже.", "Ошибка обновления");
                Application.Current.Shutdown();
            }
        }

        /*protected override void Dispose(bool disposing)
        {
            if ((!disposing ? false : components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }*/

        public void DownloadFile(string urlAddress, string location)
        {
            MainWindow main = this;
            main.currentIt = main.currentIt + 1;
            if (!Directory.Exists(string.Concat(Directory.GetCurrentDirectory(), "/temp/")))
            {
                Directory.CreateDirectory(string.Concat(Directory.GetCurrentDirectory(), "/temp/"));
            }
            Console.WriteLine(string.Concat("[SYSTEM] Загрузка начата с: ", urlAddress, " в: ", location));
            log(string.Concat("[SYSTEM] Загрузка начата с: ", urlAddress, " в: ", location));
            WebClient webClient = new WebClient();
            WebClient webClient1 = webClient;
            this.webClient = webClient;
            WebClient webClient2 = webClient1;
            try
            {
                this.webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                this.webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
                Uri URL = (urlAddress.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? new Uri(urlAddress) : new Uri(string.Concat("http://", urlAddress)));
                sw.Start();
                try
                {
                    this.webClient.DownloadFileAsync(URL, location);
                }
                catch (Exception exception)
                {
                    Exception ex = exception;
                    Console.WriteLine("[SYSTEM] Ошибка загрузки.");
                    log(string.Concat("[SYSTEM] Ошибка загрузки: ", ex.Message));
                    MessageBox.Show(ex.Message);
                }
            }
            finally
            {
                if (webClient2 != null)
                {
                    ((IDisposable)webClient2).Dispose();
                }
            }
        }

        public void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            Exception e;
            string[] strArrays = new string[] { "[SYSTEM] Распаковка файла: ", archiveFilenameIn, ", в папку: ", outFolder, " начата." };
            Console.WriteLine(string.Concat(strArrays));
            strArrays = new string[] { "[SYSTEM] Распаковка файла: ", archiveFilenameIn, ", в папку: ", outFolder, " начата." };
            log(string.Concat(strArrays));
            ZipFile zipFiles = null;
            int num2 = 0;
            try
            {
                try
                {
                    zipFiles = new ZipFile(File.OpenRead(archiveFilenameIn));
                    if (!string.IsNullOrEmpty(password))
                    {
                        zipFiles.Password = password;
                    }
                    Dispatcher.Invoke(new Action(()=> progressBar.Maximum = (int)zipFiles.Count + 1));
                    foreach (ZipEntry zipEntry in zipFiles)
                    {
                        Dispatcher.Invoke(new Action(() =>
                        {
                            ProgressBar progressBar_this = progressBar;
                            int num = num2;
                            int num1 = num;
                            num2 = num + 1;
                            progressBar_this.Value = num1;
                        }));
                        string newVersionTemp = newVersionNumber.Replace(".", "_");
                        object[] count = new object[] { "(", currentIt, " / ", totalIt, ") Установка: win_update_", newVersionTemp, ".zip ", num2, " с ",
                            (int)zipFiles.Count, " завершена." };
                        string str = string.Concat(count);
                        Dispatcher.Invoke(new Action(()=> textField.Content = str));
                        if (zipEntry.IsFile)
                        {
                            string entryFileName = zipEntry.Name;
                            byte[] buffer = new byte[4096];
                            Stream zipStream = zipFiles.GetInputStream(zipEntry);
                            string fullZipToPath = System.IO.Path.Combine(outFolder, entryFileName);
                            string directoryName = System.IO.Path.GetDirectoryName(fullZipToPath);
                            if (directoryName.Length > 0)
                            {
                                Directory.CreateDirectory(directoryName);
                            }
                            FileStream streamWriter = File.Create(fullZipToPath);
                            try
                            {
                                StreamUtils.Copy(zipStream, streamWriter, buffer);
                            }
                            finally
                            {
                                if (streamWriter != null)
                                {
                                    ((IDisposable)streamWriter).Dispose();
                                }
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    e = exception;
                    Console.WriteLine(string.Concat("[SYSTEM] Не могу установить файл. ", e.Message));
                    log(string.Concat("[SYSTEM] Не могу установить файл. ", e.Message));
                }
            }
            finally
            {
                if (zipFiles != null)
                {
                    zipFiles.IsStreamOwner = true;
                    zipFiles.Close();
                }
            }
            Console.WriteLine("[SYSTEM] Распаковка завершена.");
            log("[SYSTEM] Распаковка завершена.");
            Settings.Default.version = newVersionNumber;
            Settings.Default.Save();
            version = newVersionNumber;
           Dispatcher.Invoke(new Action(()=>Title = string.Concat("CSGO games", " v.", version)));
            if (File.Exists(archiveFilenameIn))
            {
                Console.WriteLine(string.Concat("[SYSTEM] Удаление .zip ", archiveFilenameIn));
                log(string.Concat("[SYSTEM] Удаление .zip ", archiveFilenameIn));
                try
                {
                    File.Delete(archiveFilenameIn);
                }
                catch (Exception exception1)
                {
                    e = exception1;
                    Console.WriteLine(string.Concat("[SYSTEM] Не могу установить файл. ", e.Message));
                    log(string.Concat("[SYSTEM] Не могу установить файл. ", e.Message));
                    MessageBox.Show(e.Message, "Ошибка обновления");
                }
            }
            check();
        }

        public void loadLocalVersion()
        {
            version = Settings.Default.version;
        }

        public void log(string s)
        {
            DateTime now = DateTime.Now;
            string toWrite = string.Concat("[", now.ToString("MM\\/dd\\/yyyy H:mm:ss"), "] ");
            toWrite = string.Concat(toWrite, s);
            try
            {
                StreamWriter writer = new StreamWriter("launcher_debug.txt", true);
                try
                {
                    writer.WriteLine(toWrite);
                }
                finally
                {
                    if (writer != null)
                    {
                        ((IDisposable)writer).Dispose();
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(string.Concat("[SYSTEM] Не могу записать лог в launcher_debug.txt ", exception.Message));
            }
        }

        public void play()
        {
                playButton.IsEnabled = false;
                repairButton.IsEnabled = false;
            //Settings.Default.autoPlay = this.autoPlay.Checked;
            Settings.Default.Save();
            Console.WriteLine("[SYSTEM] Начинаем игру...");
            log("[SYSTEM] Начинаем игру...");
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    FileName = string.Concat(Directory.GetCurrentDirectory(), "/game/game.exe")
                };
                Process.Start(startInfo);
                Application.Current.Shutdown();
            }
            catch (Exception exception)
            {
                Exception e = exception;
                Console.WriteLine(string.Concat("[SYSTEM] Не могу найти game.exe ", e.Message));
                log(string.Concat("[SYSTEM] Не могу найти game.exe ", e.Message));
                MessageBox.Show("Не могу найти game.exe. Может попробуйте починить?", "Ошибка начала игры.");
                Dispatcher.Invoke(new Action(() =>
                {
                    playButton.IsEnabled = true;
                    repairButton.IsEnabled = true;
                }));
            }
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            play();
        }

        private void progressBar_MouseHover(object sender, EventArgs e)
        {
            //toolTip1.Show(toolTipString, progressBar);
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            string newVersionTemp = newVersionNumber.Replace(".", "_");
            object[] objArray = new object[] { "(", currentIt, " / ", totalIt, ") Загрузка: update_", newVersionTemp, ".zip" };
            string str = string.Concat(objArray);
            double bytesReceived = (double)e.BytesReceived / 1024 / sw.Elapsed.TotalSeconds;
            string str1 = string.Format("{0} Кб/с", bytesReceived.ToString("0.00"));
            int progressPercentage = e.ProgressPercentage;
            toolTipString = string.Concat(str1, " - ", progressPercentage.ToString(), "%");
            try
            {
                Dispatcher.Invoke(new Action(() =>
                {
                    if (e.ProgressPercentage <= progressBar.Maximum)
                        progressBar.Value = e.ProgressPercentage;
                }));
            }
            catch (Exception exception)
            {
                Exception e1 = exception;
                Console.WriteLine(string.Concat("[SYSTEM] ошибка: ", e1.Message));
                log(string.Concat("[SYSTEM] ошибка: ", e1.Message));
            }
            string str2 = str;
            bytesReceived = (double)e.BytesReceived / 1024 / 1024;
            string str3 = bytesReceived.ToString("0.00");
            bytesReceived = (double)e.TotalBytesToReceive / 1024 / 1024;
            str = string.Concat(str2, " - ", string.Format("{0} Мб'с / {1} Мб'с", str3, bytesReceived.ToString("0.00")));
            try
            {
               Dispatcher.Invoke(new Action(() => textField.Content = str));
            }
            catch (Exception exception1)
            {
                Exception e2 = exception1;
                Console.WriteLine(string.Concat("[SYSTEM] ошибка: ", e2.Message));
                log(string.Concat("[SYSTEM] ошибка: ", e2.Message));
            }
        }

        private void repairButton_Click(object sender, RoutedEventArgs e)
        {
            Settings.Default.version = "NO-VERSION";
            Settings.Default.Save();

            Thread.Sleep(500);
            Process.Start(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Application.Current.Shutdown();
        }

        private void patchesComboBox_SelectedValueChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                notes.Text = patches[patchesComboBox.SelectedItem.ToString()];
            }
            catch (Exception) { }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            playButton.IsEnabled = false;
            repairButton.IsEnabled = false;
            XmlDocument xml = null;
            if (!Directory.Exists(Directory.GetCurrentDirectory() + "/Files"))
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/Files");
            if (!File.Exists(Directory.GetCurrentDirectory() + "/Files/launcher.xml"))
            {
                FileStream newFile = File.Create(Directory.GetCurrentDirectory() + "/Files/launcher.xml");
                StreamWriter SW = new StreamWriter(newFile);
                SW.Write("<configuration><url>http://tdb.host22.com/update/</url></configuration>");
                SW.Close();
            }

            try
            {
                xml = new XmlDocument();
                xml.Load(string.Concat(Directory.GetCurrentDirectory(), "/Files/launcher.xml"));
                foreach (XmlNode xn in xml.SelectNodes("/configuration"))
                {
                    url = xn["url"].InnerText;
                }
            }
            catch (Exception exception)
            {
                Exception e3 = exception;
                Console.WriteLine(string.Concat("[SYSTEM] Не найден файл конфигурации. ", e3));
                log(string.Concat("[SYSTEM] Не найден файл конфигурации. ", e3));
                Application.Current.Shutdown();
            }
            loadLocalVersion();
            //this.autoPlay.Checked = Settings.Default.autoPlay;
            Console.WriteLine(string.Concat("[SYSTEM] Проверка на новую версию... Текущая версия: ", version));
            log(string.Concat("[SYSTEM] Проверка на новую версию... Текущая версия: ", version));
            textField.Content = "Проверка на новую версию...";
            thread = new Thread(new ThreadStart(check))
            {
                IsBackground = true
            };
            thread.Start();
            //progressBar.Foreground = Color.FromRgb(255,0,0);
        }
    }
}
