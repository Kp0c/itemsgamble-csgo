﻿using System;
using System.Windows.Documents;

namespace UI
{
    public interface ITextFormatter
    {
        string GetText(FlowDocument document);
        void SetText(FlowDocument document, string text);
    }
}
