﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.Threading;

namespace Launcher
{
    public partial class UpdaterForm : Form
    {
        public UpdaterForm()
        {
            InitializeComponent();
        }

        public bool updateLauncher = false;
        public bool updateDLL = false;
        public bool updateTool = false;

        private void UpdaterForm_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 50;
            timer.Tick += (object sender2, EventArgs e2) =>
            {
                if (!updateDLL && !updateTool && !updateLauncher)
                {
                    Close();
                }
            };
            timer.Start();

            Invoke(new Action(() => progressBar.Maximum = 100));
            Invoke(new Action(() => progressBar.Value = 100));
            if (updateDLL) DownloadFile(Updater.url + @"ICSharpCode.SharpZipLib.dll",
                string.Concat(Directory.GetCurrentDirectory(), @"/ICSharpCode.SharpZipLib.dll"),false,false);
            if (updateTool)
            {
                DownloadFile(Updater.url + @"UpdateTool.zip", string.Concat(Directory.GetCurrentDirectory(), @"/temp/UpdateTool.zip"), false, true);
            }
            if(updateLauncher) DownloadFile(Updater.url+@"Launcher.zip",string.Concat(Directory.GetCurrentDirectory(), @"/temp/launcher.zip"),true,false);
        }

        public void DownloadFile(string urlAddress, string location, bool launcher, bool updateTool)
        {
            UpdaterForm main = this;
            if (!Directory.Exists(string.Concat(Directory.GetCurrentDirectory(), "/temp/")))
            {
                Directory.CreateDirectory(string.Concat(Directory.GetCurrentDirectory(), "/temp/"));
            }
            Console.WriteLine(string.Concat("[SYSTEM] Download started of: ", urlAddress, " to: ", location));
            WebClient webClient = new WebClient();
            try
            {
                if(launcher) webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(CompletedWithUpdateTool);
                else if (updateTool) webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(CompletedUpdateTool);
                else webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
                Uri URL = (urlAddress.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? new Uri(urlAddress) : new Uri(string.Concat("http://", urlAddress)));
                try
                {
                    webClient.DownloadFileAsync(URL, location);
                }
                catch (Exception exception)
                {
                    Exception ex = exception;
                    Console.WriteLine("[SYSTEM] Download Error.");
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void CompletedWithUpdateTool(object sender, AsyncCompletedEventArgs e)
        {
            updateLauncher = false;
            if (e.Cancelled) MessageBox.Show("Cancelled");
            else
            {
                if (updateDLL || updateTool || updateLauncher)
                {
                    System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                    timer.Interval = 200;
                    timer.Tick += (object sender2, EventArgs e2) =>
                    {
                        if (!updateDLL && !updateTool && !updateLauncher)
                        {
                            Process.Start(Application.StartupPath + @"/updateTool.exe");
                            Application.Exit();
                            timer.Stop();
                        }
                    };
                    timer.Start();
                }
                else
                {
                    Process.Start(Application.StartupPath + @"/updateTool.exe");
                    Application.Exit();
                }
            }
        }

        private void CompletedUpdateTool(object sender, AsyncCompletedEventArgs e)
        {
            updateTool = false;
            if (e.Cancelled) MessageBox.Show("Cancelled");
            else
            {
                if (updateDLL)
                {
                    System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                    timer.Interval = 200;
                    timer.Tick += (object sender2, EventArgs e2) =>
                    {
                        if (!updateDLL)
                        {
                            ExtractZipFile(string.Concat(Directory.GetCurrentDirectory(), @"/temp/UpdateTool.zip"), null, Directory.GetCurrentDirectory());
                            timer.Stop();
                        }
                    };
                    timer.Start();
                }
                else ExtractZipFile(string.Concat(Directory.GetCurrentDirectory(), @"/temp/UpdateTool.zip"), null, Directory.GetCurrentDirectory());
            }
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            updateDLL = false;
            if (e.Cancelled) MessageBox.Show("Cancelled");
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            int progressPercentage = e.ProgressPercentage;
            try
            {
                Invoke(new Action(() =>
                {
                    if (e.ProgressPercentage <= progressBar.Maximum)
                        progressBar.Value = e.ProgressPercentage;
                }));
            }
            catch (Exception exception)
            {
                Exception e1 = exception;
                Console.WriteLine(string.Concat("[SYSTEM] error: ", e1.Message));
            }
        }

        public void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            Exception e;
            string[] strArrays = new string[] { "[SYSTEM] Extraction of file: ", archiveFilenameIn, ", with output: ", outFolder, " started." };
            Console.WriteLine(string.Concat(strArrays));
            strArrays = new string[] { "[SYSTEM] Extraction of file: ", archiveFilenameIn, ", with output: ", outFolder, " started." };
            ZipFile zipFiles = null;
            try
            {
                try
                {
                    zipFiles = new ZipFile(File.OpenRead(archiveFilenameIn));
                    if (!string.IsNullOrEmpty(password))
                    {
                        zipFiles.Password = password;
                    }
                    foreach (ZipEntry zipEntry in zipFiles)
                    {
                        if (zipEntry.IsFile)
                        {
                            string entryFileName = zipEntry.Name;
                            byte[] buffer = new byte[4096];
                            Stream zipStream = zipFiles.GetInputStream(zipEntry);
                            string fullZipToPath = Path.Combine(outFolder, entryFileName);
                            string directoryName = Path.GetDirectoryName(fullZipToPath);
                            if (directoryName.Length > 0)
                            {
                                Directory.CreateDirectory(directoryName);
                            }
                            FileStream streamWriter = File.Create(fullZipToPath);
                            try
                            {
                                StreamUtils.Copy(zipStream, streamWriter, buffer);
                            }
                            finally
                            {
                                if (streamWriter != null)
                                {
                                    ((IDisposable)streamWriter).Dispose();
                                }
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    e = exception;
                    Console.WriteLine(string.Concat("[SYSTEM] Could not install file. ", e.Message));
                }
            }
            finally
            {
                if (zipFiles != null)
                {
                    zipFiles.IsStreamOwner = true;
                    zipFiles.Close();
                }
            }
            Console.WriteLine("[SYSTEM] Extraction complete.");
            if (File.Exists(archiveFilenameIn))
            {
                Console.WriteLine(string.Concat("[SYSTEM] Deleting .zip ", archiveFilenameIn));
                try
                {
                    File.Delete(archiveFilenameIn);
                }
                catch (Exception exception1)
                {
                    e = exception1;
                    Console.WriteLine(string.Concat("[SYSTEM] Could not install file. ", e.Message));
                }
            }
        }
    }
}
