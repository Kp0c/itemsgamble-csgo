﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.Diagnostics;

namespace UpdateTool
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("");//begin install
            ExtractZipFile(string.Concat(Directory.GetCurrentDirectory(), @"/temp/launcher.zip"), null,
                    Directory.GetCurrentDirectory());
            Process.Start(Directory.GetCurrentDirectory() + "/launcher.exe");
        }

        public static void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            Exception e;
            string[] strArrays = new string[] { "[SYSTEM] Extraction of file: ", archiveFilenameIn, ", with output: ", outFolder, " started." };
            Console.WriteLine(string.Concat(strArrays));
            strArrays = new string[] { "[SYSTEM] Extraction of file: ", archiveFilenameIn, ", with output: ", outFolder, " started." };
            ZipFile zipFiles = null;
            try
            {
                try
                {
                    zipFiles = new ZipFile(File.OpenRead(archiveFilenameIn));
                    if (!string.IsNullOrEmpty(password))
                    {
                        zipFiles.Password = password;
                    }
                    foreach (ZipEntry zipEntry in zipFiles)
                    {
                        if (zipEntry.IsFile)
                        {
                            string entryFileName = zipEntry.Name;
                            byte[] buffer = new byte[4096];
                            Stream zipStream = zipFiles.GetInputStream(zipEntry);
                            string fullZipToPath = Path.Combine(outFolder, entryFileName);
                            string directoryName = Path.GetDirectoryName(fullZipToPath);
                            if (directoryName.Length > 0)
                            {
                                Directory.CreateDirectory(directoryName);
                            }
                            FileStream streamWriter = File.Create(fullZipToPath);
                            try
                            {
                                StreamUtils.Copy(zipStream, streamWriter, buffer);
                            }
                            finally
                            {
                                if (streamWriter != null)
                                {
                                    ((IDisposable)streamWriter).Dispose();
                                }
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    e = exception;
                    Console.WriteLine(string.Concat("[SYSTEM] Could not install file. ", e.Message));
                }
            }
            finally
            {
                if (zipFiles != null)
                {
                    zipFiles.IsStreamOwner = true;
                    zipFiles.Close();
                }
            }
            Console.WriteLine("[SYSTEM] Extraction complete.");
            if (File.Exists(archiveFilenameIn))
            {
                Console.WriteLine(string.Concat("[SYSTEM] Deleting .zip ", archiveFilenameIn));
                try
                {
                    File.Delete(archiveFilenameIn);
                }
                catch (Exception exception1)
                {
                    e = exception1;
                    Console.WriteLine(string.Concat("[SYSTEM] Could not install file. ", e.Message));
                }
            }
        }
    }
}
