﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SteamTrade
{
    public class Value : IEquatable<Value>, IComparable<Value>, IComparable
    {
        public double price;

        private bool max;

        public Value(bool max = false)
        {
            this.max = max;
        }

        public double PriceFromMarketName(string MarketName, int appId = 730)
        {
            using (WebClient client = new WebClient())
            {
                string pattern = @"\b\d*[.]\d{2}?\b";
                Regex getPrice = new Regex(pattern);

                bool allow = false;
                string response = "";

                while (!allow)
                    try
                    {
                        response = client.DownloadString("http://steamcommunity.com/market/priceoverview/?country=UA&currency=1&appid=" + appId + "&market_hash_name=" + MarketName);
                        allow = true;
                    }
                    catch (System.Net.WebException) { allow = false; }

                var inventory = JsonConvert.DeserializeObject<ValueResponse>(response);

                bool getedMP = false;
                bool getedLP = false;

                double median_price = 0,
                       lowest_price = 0;

                try
                {
                    median_price = Convert.ToDouble(getPrice.Match(inventory.median_price).Value, CultureInfo.InvariantCulture);
                    getedMP = true;
                }
                catch (FormatException) { getedMP = false; }

                try
                {
                    lowest_price = Convert.ToDouble(getPrice.Match(inventory.lowest_price).Value, CultureInfo.InvariantCulture);
                    getedLP = true;
                }
                catch (FormatException) { getedLP = false; }

                if (getedLP && getedMP)
                {
                    if (max) return GetMax(median_price, lowest_price); else return GetMin(median_price, lowest_price);
                }
                else if (getedLP) return lowest_price;
                else return median_price;
            }
        }

        public int GetVolume(string marketName, int appId = 730)
        {
            using (WebClient client = new WebClient())
            {
                bool allow = false;
                string response = "";

                while (!allow)
                    try
                    {
                        response = client.DownloadString("http://steamcommunity.com/market/priceoverview/?country=UA&currency=1&appid=" + appId + "&market_hash_name=" + marketName);
                        allow = true;
                    }
                    catch (WebException) { allow = false; }

                var inventory = JsonConvert.DeserializeObject<ValueResponse>(response);

                return Convert.ToInt32(inventory.volume.Replace(",","").Replace(".",""));

            }
        }

        public double GetLowestPrice(string MarketName, int appId = 730)
        {
            using (WebClient client = new WebClient())
            {
                string pattern = @"\b\d*[.]\d{2}?\b";
                Regex getPrice = new Regex(pattern);

                bool allow = false;
                string response = "";

                while (!allow)
                    try
                    {
                        response = client.DownloadString("http://steamcommunity.com/market/priceoverview/?country=UA&currency=1&appid=" + appId + "&market_hash_name=" + MarketName);
                        allow = true;
                    }
                    catch (System.Net.WebException) { allow = false; }

                var inventory = JsonConvert.DeserializeObject<ValueResponse>(response);

                double lowest_price = 0;

                lowest_price = Convert.ToDouble(getPrice.Match(inventory.lowest_price).Value, CultureInfo.InvariantCulture);

                return lowest_price;

            }
        }

        private double GetMax(double value1, double value2)
        {
            if (value1 >= value2) return value1; else return value2;
        }

        private double GetMin(double value1, double value2)
        {
            if (value1 <= value2) return value1; else return value2;
        }

        #region operators

        public static bool operator >(Value csgoValue1, Value csgoValue2)
        {
            return csgoValue1.price > csgoValue2.price;
        }

        public static bool operator <(Value csgoValue1, Value csgoValue2)
        {
            return csgoValue1.price < csgoValue2.price;
        }

        public static bool operator >=(Value csgoValue1, Value csgoValue2)
        {
            return csgoValue1.price >= csgoValue2.price;
        }

        public static bool operator <=(Value csgoValue1, Value csgoValue2)
        {
            return csgoValue1.price <= csgoValue2.price;
        }

        public static bool operator >=(Value csgoValue1, double value2)
        {
            return csgoValue1.price >= value2;
        }

        public static bool operator <=(Value csgoValue1, double value2)
        {
            return csgoValue1.price <= value2;
        }

        public static double operator -(Value csgoValue1, Value csgoValue2)
        {
            return csgoValue1.price - csgoValue2.price;
        }

        public static double operator /(Value csgoValue1, double value2)
        {

            return csgoValue1.price / value2;
        }

        #endregion

        public void Reset()
        {
            price = 0;
        }

        public bool Equals(Value other)
        {
            return price == other.price;
        }

        public int CompareTo(Value other)
        {
            if (price == other.price)
            {
                return 1;
            }
            return 0;
        }

        public int CompareTo(object obj)
        {
            if (ReferenceEquals(null, obj) || !(obj is Value))
                return 1;
            return CompareTo((Value)obj);
        }
    }

    class ValueResponse
    {
        [JsonProperty("median_price")]
        public string median_price = "0";
        [JsonProperty("lowest_price")]
        public string lowest_price = "0";
        [JsonProperty("volume")]
        public string volume = "0";
    }
}
