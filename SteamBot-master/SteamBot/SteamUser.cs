﻿using Newtonsoft.Json;
using SteamKit2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SteamBot
{
    class SteamUserInfo
    {
        public string personaname = "[unknow]";
        public string loccountrycode = "EN";

        public SteamUserInfo(string apiKey, SteamID steamId)
        {
            using (WebClient client = new WebClient())
            {
                bool allow = false;
                string response = "";
                dynamic userResponse;

                while (!allow)
                {
                    try
                    {
                        response = client.DownloadString("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" + apiKey + "&steamids=" + steamId.ConvertToUInt64());
                        allow = true;
                    }
                    catch (WebException) { allow = false; }
                }

                userResponse = JsonConvert.DeserializeObject(response);

                foreach (var player in userResponse.response.players)
                {
                    personaname = player.personaname;
                    loccountrycode = player.loccountrycode;
                }
            }
        }

        public override string ToString()
        {

            return "PersonaName = " + personaname + "; LocCountryCode = " + loccountrycode;
        }
    }
}
