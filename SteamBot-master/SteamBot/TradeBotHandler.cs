﻿using SteamKit2;
using SteamTrade;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace SteamBot
{
    class TradeBotHandler:UserHandler
    {
        private readonly GenericInventory myGenericInventory;
        private readonly GenericInventory otherGenericInventory;

        //paths
        public const string TRADEDSOUNDPATH = "traded.wav";

        //messages
        public const string REPLYMESSAGETOSPAMMER = "fuck off!";
        public const string REPLYMESSAGETOSCAMMER = "fuck you, scammer!";

        //cases
        public const string CHROMA = "720268538_0";
        public const string CHROMA_2 = "926978479_0";
        public const string HUNT = "469431148_0";
        public const string BREAKOUT = "520025252_0";
        public const string VANGUARD = "638240019_0";

        //keys
        public const string CHROMA_KEY = "721248158_143865972";
        public const string PHOENIX_KEY = "360448780_143865972";
        public const string CSGO_KEY = "186150629_143865972";
        public const string BREAKOUT_KEY = "613589848_143865972";
        public const string HUNT_KEY = "506856210_143865972";
        public const string VANGUARD_KEY = "638243112_143865972";
        public const string WINTER_KEY = "259019412_143865972";
        public const string ESPORTS_KEY = "186150630_143865972";

        Timer timer = new Timer(60000);

        public List<TraderBot> TraderBots = new List<TraderBot>() { new TraderBot(new SteamID(76561198096366059),"RFE7GX8T"),
                                                                    new TraderBot(new SteamID(76561198096365603),"SuQMsCVr"), };

        public List<string> flagsToKick = new List<string>() { ".png",
                                                               ".jpeg",
                                                               "{LINK REMOVED}",
                                                               "screenshot",
                                                               "steam",
                                                               "stearn",};

        public List<ulong> gambleBots = new List<ulong>()
        {
            76561198089872749,
        };

        public const int MAX_PRICE = 5;
        public const int MIN_PRICE = 1;
        public const int COMMISION = 15;

        public Value myValue = new Value(true);
        public Value otherValue = new Value(false);

        private readonly CSGOSchema csGOSchema;

        public List<long> contextIds = new List<long>() { 2 };
        public List<int> appIds = new List<int>() { 730, /*570*/ };

        System.Media.SoundPlayer player;

        public TradeBotHandler(Bot bot, SteamID sid)
            : base(bot, sid)
        {
            myGenericInventory = new GenericInventory(SteamWeb);
            otherGenericInventory = new GenericInventory(SteamWeb);
            csGOSchema = CSGOSchema.FetchSchema(Bot.ApiKey);
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            player = new System.Media.SoundPlayer(TRADEDSOUNDPATH);
            player.Load();
        }

        public override bool OnGroupAdd()
        {
            return false;
        }

        public override bool OnFriendAdd()
        {
            return true;
        }

        public override void OnFriendRemove() { }

        public void LogTrade(SteamTrade.TradeOffer.TradeOffer offer, string error)
        {
            return;
            string path = Path.Combine("TradeLogs");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path = Path.Combine(path, DateTime.Now.ToShortDateString());
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path = Path.Combine(path, DateTime.Now.ToShortTimeString().Replace(":",";")+".txt");
            if (!File.Exists(path))
            {
                File.Create(path);
            }

            //CSGOInventory myPriceHelper = CSGOInventory.FetchInventory(Bot.SteamClient.SteamID.ConvertToUInt64());
            //CSGOInventory otherPriceHelper = CSGOInventory.FetchInventory(offer.PartnerSteamId.ConvertToUInt64());
            //otherGenericInventory.load(730, contextIds, offer.PartnerSteamId);
            //myGenericInventory.load(730, contextIds, Bot.SteamClient.SteamID);
            bool Access = false;
            StreamWriter SW = null;
            GenericInventory.ItemDescription description;

            int tryes = 0;
            while (!Access)
            {
                try
                {
                    SW = new StreamWriter(path);
                    Access = true;
                }
                catch (Exception)
                {
                    if (tryes >= 100) return;
                    //Log.Info(ex.Message);
                    Access = false;
                    tryes++;
                }
            }
            SteamUserInfo otherSteamUser = new SteamUserInfo(Bot.ApiKey, offer.PartnerSteamId);
            //SW.WriteLine("~~~Trade with SteamID {0}~~~",offer.PartnerSteamId.ConvertToUInt64().ToString());
            SW.WriteLine("~~~Trade with {0} from [{1}]~~~",otherSteamUser.personaname,otherSteamUser.loccountrycode);
            SW.WriteLine();

            SW.WriteLine("My items: ");
            double myTotalPrice = 0;
            for (int i = 0; i < offer.Items.GetMyItems().Count; i++)
            {
                description = myGenericInventory.getDescription((ulong)offer.Items.GetMyItems()[i].AssetId);
                if (description.marketable)
                {
                    //string marketName = myPriceHelper.getMarketNameFromAssetId(offer.Items.GetMyItems()[i].AssetId);
                    string marketName = myGenericInventory.getDescription((ulong)offer.Items.GetMyItems()[i].AssetId).market_name;
                    myTotalPrice += myValue.PriceFromMarketName(marketName, (int)offer.Items.GetMyItems()[i].AppId);
                    SW.WriteLine("{0}) {1}. Price: {2}", i + 1, marketName, myValue.PriceFromMarketName(marketName, (int)offer.Items.GetMyItems()[i].AppId));
                }
            }
            SW.WriteLine();

            SW.WriteLine("Other items: ");
            double otherTotalPrice = 0;
            for (int i = 0; i < offer.Items.GetTheirItems().Count; i++)
            {
                description = otherGenericInventory.getDescription((ulong)offer.Items.GetTheirItems()[i].AssetId);
                if (description.marketable)
                {
                    //string marketName = otherPriceHelper.getMarketNameFromAssetId(offer.Items.GetTheirItems()[i].AssetId);
                    string marketName = otherGenericInventory.getDescription((ulong)offer.Items.GetTheirItems()[i].AssetId).market_name;
                    otherTotalPrice += otherValue.PriceFromMarketName(marketName, (int)offer.Items.GetTheirItems()[i].AppId);
                    SW.WriteLine("{0}) {1}. Price: {2}", i + 1, marketName, otherValue.PriceFromMarketName(marketName, (int)offer.Items.GetTheirItems()[i].AppId));
                }
            }
            SW.WriteLine();

            SW.WriteLine("Income: {0}",otherTotalPrice-myTotalPrice);

            if (error != "")
            {
                SW.WriteLine("And i have an error - {0}",error);
            }

            SW.Close();

            File.Move(path, path + "   income - " + (otherTotalPrice - myTotalPrice).ToString()+".txt");
        }

        public override void OnNewTradeOffer(SteamTrade.TradeOffer.TradeOffer offer)
        {
            if (Bot.IsLoggedIn && !gambleBots.Contains(offer.PartnerSteamId.ConvertToUInt64()))
            {
               // int dontStat = 0;
                otherGenericInventory.load(appIds, contextIds, offer.PartnerSteamId);
                myGenericInventory.load(appIds, contextIds, Bot.SteamClient.SteamID);

                myValue.Reset();
                otherValue.Reset();

                //CSGOInventory myPriceHelper = CSGOInventory.FetchInventory(Bot.SteamClient.SteamID.ConvertToUInt64());
                //CSGOInventory otherPriceHelper = CSGOInventory.FetchInventory(offer.PartnerSteamId.ConvertToUInt64());

                //.................................................TEST.................................................................
                //if (IsAdmin)
                //{
                //    foreach (SteamTrade.TradeOffer.TradeOffer.TradeStatusUser.TradeAsset asset in offer.Items.GetTheirItems())
                //    {
                //        if (asset.AppId == 730)
                //        {

                //            Log.Info(otherGenericInventory.getDescription((ulong)asset.AssetId).market_name);
                //            otherValue.price += otherValue.PriceFromMarketName(otherGenericInventory.getDescription((ulong)asset.AssetId).market_name);
                //        }
                //    }
                //}
                //else

                List<string> ignoredItems = new List<string>();
                if (offer.Items.GetTheirItems().Count != 0)
                    {

                        #region addPrice value
                        foreach (SteamTrade.TradeOffer.TradeOffer.TradeStatusUser.TradeAsset asset in offer.Items.GetTheirItems())
                        {

                        if (appIds.Contains((int)asset.AppId))
                        {
                            //otherValue.price += otherValue.PriceFromMarketName(otherPriceHelper.getMarketNameFromAssetId(asset.AssetId));
                            double price = otherValue.PriceFromMarketName(otherGenericInventory.getDescription((ulong)asset.AssetId).market_name, (int)asset.AppId);

                            if (price <= 0.04) price = 0.01;
                            if (price <= 0.10 && price > 0.04) price -= 0.02; ;
                            

                            double volume = otherValue.GetVolume(otherGenericInventory.getDescription((ulong)asset.AssetId).market_name,(int)asset.AppId);

                            if (volume > 20)
                                otherValue.price += price;
                            else if (volume <= 20 && volume > 1 && otherGenericInventory.getDescription((ulong)asset.AssetId).type == "★ Covert Knife")
                            {
                                otherValue.price += price;
                            }
                            else ignoredItems.Add(otherGenericInventory.getDescription((ulong)asset.AssetId).market_name);

                            #region disable csgotm
                            /*
                            foreach (GenericInventory.Item item in otherGenericInventory.items.Values)
                            {
                                if (item.assetid == (ulong)asset.AssetId)
                                {
                                    if ((item.descriptionid == HUNT) || (item.descriptionid == BREAKOUT) || (item.descriptionid == VANGUARD))
                                    {
                                        dontStat++;
                                    }
                                }
                            }
                             */
                            #endregion


                        }
                        }

                        foreach (SteamTrade.TradeOffer.TradeOffer.TradeStatusUser.TradeAsset asset in offer.Items.GetMyItems())
                        {
                            if (appIds.Contains((int)asset.AppId))
                            {
                                //myValue.price += myValue.PriceFromMarketName(myPriceHelper.getMarketNameFromAssetId(asset.AssetId));
                                myValue.price += myValue.PriceFromMarketName(myGenericInventory.getDescription((ulong)asset.AssetId).market_name,(int)asset.AppId);
                            }
                            else
                            {
                                offer.Decline();
                                Log.Error("Меня пытались развести");
                                return;
                            }
                        }

                        #endregion

                       // if (offer.Items.GetMyItems().Count == 0 && offer.Items.GetTheirItems().Count == dontStat)
                       // {
                       //     Log.Info("Trade with csgo bot?");
                       //     Statistic.getedCasesFromBot++;
                       //     offer.Accept();
                       // }
                       // else
                       // {
                            Log.Info("REAL My value: {0}, other value: {1}", myValue.price, otherValue.price);
                            double realOtherValue = otherValue.price;
                            otherValue.price = otherValue.price / 100 * (100 - COMMISION);
                            Log.Info("My value: {0}, other value: {1}", myValue.price, otherValue.price);

                            if (myValue < otherValue)
                            {
                                Log.Success("Success offer: my value - {0}, other value - {1}, income - {2}", myValue.price, otherValue.price, otherValue - myValue);
                                player.Play();

                                string error = "";
                                try
                                {
                                    offer.Accept();
                                }
                                catch (Exception ex)
                                {
                                    error = ex.Message;
                                }
                                finally
                                {
                                    LogTrade(offer,error);
                                }
                            }
                            else
                            {
                                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                Log.Error("Unluck offer: my value - {0}, other value - {1}, losses - {2}", myValue.price, otherValue.price, myValue - otherValue);
                        if (ignoredItems.Count > 0)
                        {
                            Log.Error("IGNORED ITEMS:");
                            int i = 0;
                            foreach(string item in ignoredItems)
                            {
                                Log.Error("{0}) {1}", ++i, ignoredItems[i - 1]);
                            }

                            offer.Decline();
                            return;
                        }
                                //Decline offer
                                //offer.Decline();

                                //CounterOffer
                                double realMyValue = myValue.price;
                                while (myValue >= otherValue / 1.25)
                                {
                                    foreach (SteamTrade.TradeOffer.TradeOffer.TradeStatusUser.TradeAsset asset in offer.Items.GetMyItems())
                                    {
                                        offer.Items.RemoveMyItem(730, contextIds[0], asset.AssetId);
                                        //myValue.price -= myValue.PriceFromMarketName(myPriceHelper.getMarketNameFromAssetId(asset.AssetId));
                                        myValue.price -= myValue.PriceFromMarketName(myGenericInventory.getDescription((ulong)asset.AssetId).market_name);
                                        break;
                                    }
                                }

                                if (myValue < otherValue)
                                {
                                    Log.Info("Send counter offer with my value - {0}, other value {1}, income - {2}.", myValue.price, otherValue.price, otherValue - myValue);
                                    string counterOfferId = "";
                                    offer.CounterOffer(out counterOfferId, "Sorry i can't accept your offer, because your value is " + otherValue.price + ", my value - " + realMyValue + ". Please, decline this offer!");
                                }
                            }
                       // }
                    }

                base.OnNewTradeOffer(offer);
            }
        }

        public void SendBotOffer(int price, TraderBot otherTraderBot = null)
        {
            if (otherTraderBot == null) otherTraderBot = TraderBots[GlobalVariables.currentBot];
            SteamTrade.TradeOffer.TradeOffer offer;
            bool continueOffer = true;
            foreach (OfferIdWithCount offerId in GlobalVariables.offersBots)
            {
                Bot.TryGetTradeOffer(offerId.offerId, out offer);
                if (offer.PartnerSteamId == otherTraderBot.steamID)
                {
                    continueOffer = false;
                }
            }
            if (continueOffer)
            {
                offer = Bot.NewTradeOffer(otherTraderBot.steamID);

                int Cases = 0;
                int maxCases = 0;

                myGenericInventory.load(appIds, contextIds, Bot.SteamClient.SteamID);
                otherGenericInventory.load(appIds, contextIds, otherTraderBot.steamID);

                foreach (GenericInventory.Item item in myGenericInventory.items.Values)
                    if ((item.appid == 730) && (item.descriptionid == HUNT || item.descriptionid == BREAKOUT || item.descriptionid == VANGUARD))
                    {
                        maxCases++;
                    }

                foreach (GenericInventory.Item item in otherGenericInventory.items.Values)
                {
                    if (item.descriptionid == CHROMA && maxCases >= price)
                    {
                        offer.Items.AddTheirItem(730, item.contextid, (long)item.assetid);
                        Cases += price;
                        maxCases -= price;
                    }
                }
                if (Cases > 0)
                {
                    foreach (GenericInventory.Item item in myGenericInventory.items.Values)
                    {
                        if ((item.appid == 730) && (item.descriptionid == HUNT || item.descriptionid == BREAKOUT || item.descriptionid == VANGUARD))
                        {
                            if (Cases >= 1)
                            {
                                offer.Items.AddMyItem(730, item.contextid, (long)item.assetid);
                                Cases -= 1;
                            }
                            else break;
                        }

                    }
                    if (Cases == 0)
                    {
                        string newOfferId;
                        if (offer.SendWithToken(out newOfferId, otherTraderBot.token, "no"))
                        {
                            //Log.Info("Trade offer sent. Price: " + price);
                            GlobalVariables.offersBots.Add(new OfferIdWithCount(otherTraderBot, offer.Items.GetTheirItems().Count, price, newOfferId));
                        }
                    }
                    else
                    {
                        Log.Error("I don't have so much Cases! Please add {0} cases!", Cases);
                    }
                }
            }
            GlobalVariables.currentBot++;
            if (GlobalVariables.currentBot > TraderBots.Count - 1)
            {
                GlobalVariables.currentBot = 0;
            }

        }

        public void CheckOffers()
        {
            try
            {
                foreach (OfferIdWithCount offerId in GlobalVariables.offersBots)
                {
                    SteamTrade.TradeOffer.TradeOffer offer;
                    Bot.TryGetTradeOffer(offerId.offerId, out offer);
                    if (offer.OfferState != SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateActive)
                    {
                        if (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateAccepted)
                        {
                            Log.Success("я получил {0} хромов по цене - {2}", offerId.count,  offerId.price);
                            GlobalVariables.offersBots.Remove(offerId);
                        }
                        else if ((offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateDeclined) ||
                          (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateCountered))
                        {
                            if (offerId.price < MAX_PRICE)
                            {
                                offerId.price++;
                                //Log.Info("Бот {0} отменил сделку, повторим попытку с ценой {1}", offer.PartnerSteamId, offerId.price);

                                GlobalVariables.offersBots.Remove(offerId);
                                SendBotOffer(offerId.price, offerId.traderBot);
                            }
                            else
                            {
                                Log.Error("Бот {0} хочет больше {1} сундуков :/", offer.PartnerSteamId, MAX_PRICE);
                                GlobalVariables.offersBots.Remove(offerId);
                            }
                        }
                        else if (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateInvalidItems)
                        {
                            Log.Error("Invalid items");
                            GlobalVariables.offersBots.Remove(offerId);
                            SendBotOffer(offerId.price, offerId.traderBot);
                        }
                        else
                        {
                            Log.Error("offerId {0} state: {1}", offerId.offerId, offer.OfferState);
                        }
                    }
                }
            }
            catch (InvalidOperationException) { }
        }

        public void TimerTick(object source, ElapsedEventArgs e)
        {
            GlobalVariables.ResetChatUsers();
            //CheckOffers();
            //SendBotOffer(MIN_PRICE);
        }

        public override void OnLoginCompleted()
        {
            if (Bot.AutoStartTimer)
            {
                //SendBotOffer(MIN_PRICE);

                timer.AutoReset = true;
                timer.Start();
                timer.Elapsed += TimerTick;
                GlobalVariables.timerStarted = true;
                Log.Info("Auto start timer enabled");
            }
        }

        public double CalculatePrice()
        {
            Value value = new Value(false);
            GenericInventory.ItemDescription description;
            int calculatedItems = 0;

            myGenericInventory.load(appIds, contextIds, Bot.SteamClient.SteamID);

            foreach(GenericInventory.Item item in myGenericInventory.items.Values){
                description = myGenericInventory.getDescription((ulong)item.assetid);
                if (description.marketable)
                {
                value.price += value.PriceFromMarketName(myGenericInventory.getDescription((ulong)item.assetid).market_name,item.appid);
                calculatedItems++;
                }
            }

            Log.Success("\nCALCULATED VALUE - {0}\nCALCULATED ITEMS - {1}", value.price,calculatedItems);
            return value.price;
        }

        public double GetUnder(double maxPrice,SteamID steamId)
        {
            Value value = new Value(false);
            GenericInventory.ItemDescription description;

            SteamTrade.TradeOffer.TradeOffer offer = Bot.NewTradeOffer(steamId);

            myGenericInventory.load(appIds, contextIds, Bot.SteamClient.SteamID);

            foreach (GenericInventory.Item item in myGenericInventory.items.Values)
            {
                description = myGenericInventory.getDescription((ulong)item.assetid);
                if (description.marketable /*&& description.type != "Base Grade Container"*/)
                {
                    double price = value.PriceFromMarketName(myGenericInventory.getDescription((ulong)item.assetid).market_name, item.appid);
                    if (price <= maxPrice)
                    {
                        value.price += price;
                        offer.Items.AddMyItem(item.appid, item.contextid,(long) item.assetid);
                    }
                }
            }
            string offerId;
            offer.Send(out offerId);

            return value.price;
        }

        #region Messages
        public override void OnChatRoomMessage(SteamID chatID, SteamID sender, string message)
        {  
            Log.Info(Bot.SteamFriends.GetFriendPersonaName(sender) + ": " + message);
            base.OnChatRoomMessage(chatID, sender, message);
        }

        public override void OnMessage(string message, SteamKit2.EChatEntryType type, SteamID sender)
        {
            string[] miniMessages = message.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            bool isKick = false;
            foreach (string flag in flagsToKick)
            {
                if (message.Contains(flag))
                {
                    isKick = true;
                }
            }

            if (isKick)
            {
                Log.Success("Scammer {0} is removed!", Bot.SteamFriends.GetFriendPersonaName(sender));
                SendReplyMessage(REPLYMESSAGETOSCAMMER);
                Bot.SteamFriends.BanChatMember(sender, sender);
                Bot.SteamFriends.RemoveFriend(sender);
            }
            else
            {
                switch (miniMessages[0])
                {
                    case "price":
                        //CalculatePrice();
                        SendReplyMessage(CalculatePrice().ToString());
                        break;
                    case "get":
                        if (IsAdmin)
                        {
                            switch (miniMessages[1])
                            {
                                case "under":
                                    miniMessages[2].Replace(",", ".");

                                    double maxPrice = Convert.ToDouble(miniMessages[2], CultureInfo.InvariantCulture);
                                    SendReplyMessage("I'll sent u " + GetUnder(maxPrice, sender) + "$");
                                    break;
                            }
                        }
                        break;
                    case "soundTest":
                        player.Play();
                        break;
                    case "NameTest":
                        SteamUserInfo otherSteamUser = new SteamUserInfo(Bot.ApiKey, sender);
                        Log.Success(otherSteamUser.ToString());
                        break;
                    case "check":
                        Value value = new Value();
                        string name = message.Remove(0, 6);

                        Log.Success(value.GetVolume(name).ToString());
                        break;
                    default:
                        int count;
                        if (GlobalVariables.chatUsers.TryGetValue(sender, out count))
                        {
                            if (count > 0) count++;
                            if (count >= 5) SendReplyMessage(REPLYMESSAGETOSPAMMER);
                            GlobalVariables.chatUsers[sender] = count;
                        }
                        else
                        {
                            GlobalVariables.chatUsers.Add(sender, 1);
                            SendReplyMessage(Bot.ChatResponse);

                        }
                        break;
                }
            }
        }

        #endregion

        #region TODO: Trade
        public override bool OnTradeRequest()
        {
            SendReplyMessage("STEAM OFFER!!");
            return false;
        }

        public override void OnTradeError(string error)
        {
            SendChatMessage("Oh, there was an error: {0}.", error);
            Log.Warn(error);
        }

        public override void OnTradeTimeout()
        {
            SendChatMessage("Sorry, but you were AFK and the trade was canceled.");
            Log.Info("User was kicked because he was AFK.");
        }

        public override void OnTradeInit()
        {
            SendTradeMessage("Success. Please put up your items.");
        }

        public override void OnTradeAddItem(CSGOSchema.Item schemaItem, Inventory.Item inventoryItem)
        {
            //Log.Success(schemaItem.ItemTypeName);
            // SendTradeMessage(schemaItem.ItemName);
        }

        public override void OnTradeRemoveItem(CSGOSchema.Item schemaItem, Inventory.Item inventoryItem) { }

        public override void OnTradeMessage(string message) { }

        public override void OnTradeReady(bool ready)
        {
            /*if (!ready)
            {
                Trade.SetReady (false);
            }
            else
            {
                if(Validate ())
                {
                    Trade.SetReady (true);
                }
                SendTradeMessage("Scrap: {0}", AmountAdded.ScrapTotal);
            }*/
        }

        public override void OnTradeSuccess()
        {
            // Trade completed successfully
            Log.Success("Trade Complete.");
        }


        public override void OnTradeAccept()
        {
            /*if (Validate() || IsAdmin)
            {
                //Even if it is successful, AcceptTrade can fail on
                //trades with a lot of items so we use a try-catch
                try {
                    if (Trade.AcceptTrade())
                        Log.Success("Trade Accepted!");
                }
                catch {
                    Log.Warn ("The trade might have failed, but we can't be sure.");
                }
            }*/
        }
        #endregion
    }
}
