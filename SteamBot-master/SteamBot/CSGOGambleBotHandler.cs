﻿using SteamKit2;
using SteamTrade;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using PlayerIOClient;
using Timer = System.Timers.Timer;

namespace SteamBot
{
    class CSGOGambleBotHandler : UserHandler
    {
        private readonly GenericInventory myGenericInventory;
        private readonly GenericInventory otherGenericInventory;

        Timer timer = new Timer(10000);
        Value value = new Value(false);

        public List<long> contextIds = new List<long>() { 2 };
        public List<int> appIds = new List<int>() { 730, /*570*/ };

        Client player;

        public CSGOGambleBotHandler(Bot bot, SteamID sid)
            : base(bot, sid)
        {
            myGenericInventory = new GenericInventory(SteamWeb);
            otherGenericInventory = new GenericInventory(SteamWeb);
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            // PlayerIO.QuickConnect.SimpleConnect("items-gumble-test-grbrbu1o0o8enubebcyw", "Bot", "124578", null, Success, Error);
            PlayerIO.Authenticate("items-gumble-test-grbrbu1o0o8enubebcyw", "bot", new Dictionary<string, string> {
                        { "username", "bot" },
                        { "password", "124578"},
                },
    null, Success, Error);
        }

        public void Success(Client client)
        {
            player = client;
            Check();
            CheckAccept();
            Log.Success("Loged to player.io service");

            Dictionary<string, string> roomData = new Dictionary<string, string>(){
            {"BotsCount","0"},
        };

            client.Multiplayer.CreateJoinRoom("bots", "BotRoom", true, roomData, null);
            timer.Start();
        }

        public override void OnLoginCompleted()
        {
            timer.AutoReset = true;
            timer.Elapsed += TimerTick;
        }

        public void TimerTick(object source, ElapsedEventArgs e)
        {
            Check();
            CheckAccept();
            CheckGet();
            CheckGetAccept();
        }

        #region dont use it
        public void CheckGet()
        {
            if (player != null)
            {
                player.BigDB.LoadRange("GetOffers", "BySteamId", null, null, null, 10, SuccessGetOffer, Error);
            }
        }

        public void Check()
        {
            if (player != null)
            {
                player.BigDB.LoadRange("Offers", "BySteamId", null, null, null,10, Success, Error);
            }
        }

        public void CheckAccept()
        {
            if (player != null)
            {
                player.BigDB.LoadRange("CheckAccept", "ByOfferId", null, null, null, 10, SuccessCheckAccepted, Error);
            }
        }

        public void CheckGetAccept()
        {
            if (player != null)
            {
                player.BigDB.LoadRange("GetCheckAccept", "ByOfferId", null, null, null, 10, SuccessGetCheckAccepted, Error);
            }
        }
        #endregion

        public void SuccessGetCheckAccepted(DatabaseObject[] results)
        {
            foreach (DatabaseObject DBObj in results)
            {
                Log.Success("success get check accepted");
                try
                {
                    SteamTrade.TradeOffer.TradeOffer offer;
                    Bot.TryGetTradeOffer(DBObj.GetString("offerId"), out offer);

                    if (offer != null)
                    {
                        if (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateAccepted)
                        {
                            // player.PayVault.Credit(Convert.ToUInt16(DBObj.GetInt("value")), "from " + DBObj.GetString("user"));
                            player.BigDB.LoadSingle("PlayerObjects", "BySteamId", new object[] { DBObj.GetString("user") }, delegate(DatabaseObject obj)
                            {
                                object q;
                                if (!obj.TryGetValue("serverMoney", out q))
                                {
                                    obj.Set("serverMoney", 0);
                                    obj.Save();
                                }
                                if (obj.GetInt("serverMoney") >= DBObj.GetInt("value"))
                                {
                                    obj.Set("money", obj.GetInt("money") - DBObj.GetInt("value"));
                                    obj.Set("serverMoney", obj.GetInt("serverMoney") - DBObj.GetInt("value"));
                                    obj.Save();
                                    Log.Success("Remove money");
                                    player.PayVault.Debit((uint)DBObj.GetInt("value"), "to "+DBObj.GetString("user"));
                                    foreach (string item in DBObj.GetArray("items"))
                                    {
                                        player.BigDB.DeleteKeys("Items", item);
                                    }
                                    player.BigDB.DeleteKeys("GetCheckAccept", DBObj.Key);
                                }
                                else
                                {
                                    Log.Error("havent money");
                                }
                            }, Error);
                        }
                        if (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateDeclined ||
                            offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateCountered)
                        {
                            player.BigDB.DeleteKeys("GetCheckAccept", DBObj.Key);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                }

            }
        }


        public void SuccessCheckAccepted(DatabaseObject[] results)
        {

            foreach (DatabaseObject DBObj in results)
            {
                Log.Success("success check accepted");
                try
                {
                    SteamTrade.TradeOffer.TradeOffer offer;
                    Bot.TryGetTradeOffer(DBObj.GetString("offerId"), out offer);

                    if (offer != null)
                    {
                        if (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateAccepted)
                        {
                            myGenericInventory.load(appIds, contextIds, Bot.SteamClient.SteamID);
                            // player.PayVault.Credit(Convert.ToUInt16(DBObj.GetInt("value")), "from " + DBObj.GetString("user"));
                            player.BigDB.LoadSingle("PlayerObjects", "BySteamId", new object[] { DBObj.GetString("user") }, delegate(DatabaseObject obj)
                            {
                                obj.Set("money", obj.GetInt("money") + DBObj.GetInt("value"));
                                object q;
                                if (!obj.TryGetValue("serverMoney", out q))
                                {
                                    obj.Set("serverMoney", 0);
                                    obj.Save();
                                }
                                obj.Set("serverMoney", obj.GetInt("serverMoney") + DBObj.GetInt("value"));
                                obj.Save();

                                Log.Success("added money");

                                player.PayVault.Credit((uint)DBObj.GetInt("value"), "from " + DBObj.GetString("user"));

                                player.BigDB.DeleteKeys("CheckAccept", DBObj.Key);
                            }, (PlayerIOError err) => Log.Error(err.Message));
                            foreach (DatabaseObject obj in DBObj.GetArray("items"))
                            {
                                //player.BigDB.CreateObject("Items", obj.GetString("class_id") + "_" + obj.GetString("instance_id")+"_"+DateTime.UtcNow.Ticks, obj);
                                string key = myGenericInventory.items.Values.First(id =>
                                    id.descriptionid == obj.GetString("class_id") + "_" + obj.GetString("instance_id")).assetid.ToString();
                                myGenericInventory.items.Remove(Convert.ToUInt64(key));
                                player.BigDB.CreateObject("Items", key, obj);
                            }
                        }
                        if (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateDeclined ||
                            offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateCountered)
                        {
                            player.BigDB.DeleteKeys("CheckAccept", DBObj.Key);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                }
            }

        }

        public void Error(PlayerIOError error)
        {
            Log.Error(error.ToString());
        }

        public void Success(DatabaseObject[] results)
        {
            foreach (DatabaseObject DBObj in results)
            {
                Log.Success("success send offer");
                try
                {
                    string steamId = DBObj.GetString("steamId");

                    otherGenericInventory.load(appIds, contextIds, new SteamID(Convert.ToUInt64(steamId)));

                    SteamTrade.TradeOffer.TradeOffer offer = Bot.NewTradeOffer(new SteamID(Convert.ToUInt64(steamId)));

                    int price = 0;
                    foreach (string item in DBObj.GetArray("items"))
                    {
                        long itemId = Convert.ToInt64(item);
                        price += Convert.ToInt16(value.GetLowestPrice(otherGenericInventory.getDescription((ulong)itemId).market_name) * 100);
                        offer.Items.AddTheirItem(730, 2, itemId);
                    }
                    string offerId;
                    string offerLink = DBObj.GetString("offerLink");
                    offerLink = offerLink.Substring(offerLink.Length - 8, 8);

                    offer.SendWithToken(out offerId, offerLink, "You will get " + price.ToString() + " money");

                    if (offerId != "")
                    {
                        Log.Success("Sended offer! Value - " + price.ToString() + ". OfferId - " + offerId);
                        player.BigDB.DeleteKeys("Offers", DBObj.Key);
                        DatabaseObject obj = new DatabaseObject();
                        obj.Set("offerId", offerId);
                        obj.Set("value", price);
                        obj.Set("user", steamId);
                        obj.Set("items", new DatabaseArray());

                        for (int i = 0; i < DBObj.GetArray("items").Count; i++)
                        {
                            string item = (string)DBObj.GetArray("items")[i];
                            obj.Set("items." + i + ".id", item);
                            obj.Set("items." + i + ".class_id", otherGenericInventory.getDescription((ulong)Convert.ToInt64(item)).classid.ToString());
                            obj.Set("items." + i + ".instance_id", otherGenericInventory.getDescription((ulong)Convert.ToInt64(item)).instanceid);
                            obj.Set("items." + i + ".name", otherGenericInventory.getDescription((ulong)Convert.ToInt64(item)).market_name);
                            obj.Set("items." + i + ".iconUrl", @"http://steamcommunity-a.akamaihd.net/economy/image/" + otherGenericInventory.getDescription((ulong)Convert.ToInt64(item)).icon_url);
                            obj.Set("items." + i + ".price", Convert.ToInt16(value.GetLowestPrice(otherGenericInventory.getDescription((ulong)Convert.ToInt64(item)).market_name) * 100));
                        }

                        player.BigDB.CreateObject("CheckAccept", steamId, obj);
                    }

                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                }

            }
        }

        public override void OnNewTradeOffer(SteamTrade.TradeOffer.TradeOffer offer)
        {
            if (IsAdmin)
            {
                offer.Accept();
            }
            else
            {
                offer.Decline();
            }
        }

        public void SuccessGetOffer(DatabaseObject[] results)
        { 
            foreach (DatabaseObject DBObj in results)
            {
                Log.Success("SuccessGetOffer");
                try
                {
                    string steamId = DBObj.GetString("steamId");

                    myGenericInventory.load(appIds, contextIds, Bot.SteamClient.SteamID);

                    SteamTrade.TradeOffer.TradeOffer offer = Bot.NewTradeOffer(new SteamID(Convert.ToUInt64(steamId)));


                    int price = 0;
                    foreach (string item in DBObj.GetArray("items"))
                    {
                        //long itemId = (long)myGenericInventory.items.Values.First(id => id.descriptionid.Contains("item")).assetid;
                        //price += Convert.ToInt16(value.GetLowestPrice(myGenericInventory.getDescription((ulong)itemId).market_name) * 100);
                        // DatabaseObject itemFromBD = player.BigDB.Load("Items", myGenericInventory.getDescription((ulong)itemId).classid + "_"
                        //     + myGenericInventory.getDescription((ulong)itemId).instanceid);
                        DatabaseObject itemFromBD = player.BigDB.Load("Items", item);
                        price += itemFromBD.GetInt("price");
                        offer.Items.AddMyItem(730, 2, Convert.ToInt64(item));
                    }
                    string offerId;
                    string offerLink = DBObj.GetString("offerLink");
                    offerLink = offerLink.Substring(offerLink.Length - 8, 8);

                    player.BigDB.LoadSingle("PlayerObjects", "BySteamId", new object[] { steamId }, delegate(DatabaseObject user)
                    {
                        object q;
                        if (!user.TryGetValue("serverMoney", out q))
                        {
                            user.Set("serverMoney", 0);
                            user.Save();
                        }
                        if ((user.GetInt("serverMoney") - price) >= 0)
                        {
                            offer.SendWithToken(out offerId, offerLink, "You will spend " + price.ToString() + " money");

                            if (offerId != "")
                            {
                                Log.Success("Sended offer to get! Value - " + price.ToString() + ". OfferId - " + offerId);
                                player.BigDB.DeleteKeys("GetOffers", DBObj.Key);
                                DatabaseObject obj = new DatabaseObject();
                                obj.Set("offerId", offerId);
                                obj.Set("value", price);
                                obj.Set("user", steamId);
                                //obj.Set("items", new DatabaseArray());
                                DatabaseArray items = new DatabaseArray();

                                for (int i = 0; i < DBObj.GetArray("items").Count; i++)
                                {
                                    // obj.Set("items." + i + ".id", DBObj.GetArray("items")[i].ToString());
                                    items.Add(DBObj.GetArray("items")[i].ToString());
                                }

                                obj.Set("items", items);

                                player.BigDB.CreateObject("GetCheckAccept", steamId, obj);
                            }
                        }
                        else
                        {
                            Log.Error("User haven't money");
                        }
                    });
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                }
            }

        }


        #region don't nedded



        public override void OnMessage(string message, SteamKit2.EChatEntryType type, SteamKit2.SteamID sender)
        {


        }

        public override bool OnTradeRequest()
        {
            return false;
        }

        public override void OnTradeError(string error)
        {
            SendChatMessage("Oh, there was an error: {0}.", error);
            Bot.Log.Warn(error);
        }

        public override void OnTradeTimeout()
        {
            SendChatMessage("Sorry, but you were AFK and the trade was canceled.");
            Bot.Log.Info("User was kicked because he was AFK.");
        }

        public override void OnTradeSuccess()
        {
            throw new NotImplementedException();
        }

        public override void OnTradeInit()
        {
            // NEW -------------------------------------------------------------------------------
            List<long> contextId = new List<long>();

            /*************************************************************************************
             * 
             * SteamInventory AppId = 753 
             * 
             *  Context Id      Description
             *      1           Gifts (Games), must be public on steam profile in order to work.
             *      6           Trading Cards, Emoticons & Backgrounds. 
             *  
             ************************************************************************************/

            contextId.Add(2);

            myGenericInventory.load(753, contextId, Bot.SteamClient.SteamID);
            otherGenericInventory.load(753, contextId, OtherSID);

            if (!myGenericInventory.isLoaded || !otherGenericInventory.isLoaded)
            {
                SendTradeMessage("Couldn't open an inventory, type 'errors' for more info.");
            }

            SendTradeMessage("Add yours items.");
            // -----------------------------------------------------------------------------------
        }

        public override void OnTradeAddItem(CSGOSchema.Item schemaItem, Inventory.Item inventoryItem)
        {
            // USELESS DEBUG MESSAGES -------------------------------------------------------------------------------
            /* SendTradeMessage("Object AppID: {0}", inventoryItem.AppId);
             SendTradeMessage("Object ContextId: {0}", inventoryItem.ContextId);

             switch (inventoryItem.AppId)
             {
                 case 730:
                     SendTradeMessage("CSGO Item Added.");
                     SendTradeMessage("Name: {0}", schemaItem.Name);
                     SendTradeMessage("Quality: {0}", inventoryItem.Quality);
                     SendTradeMessage("Level: {0}", inventoryItem.Level);
                     SendTradeMessage("Craftable: {0}", (inventoryItem.IsNotCraftable ? "No" : "Yes"));
                     break;

                 default:
                     SendTradeMessage("Unknown item");
                     break;
             }
             // ------------------------------------------------------------------------------------------------------*/
        }

        public override void OnTradeRemoveItem(SteamTrade.CSGOSchema.Item schemaItem, SteamTrade.Inventory.Item inventoryItem)
        {
            throw new NotImplementedException();
        }

        public override void OnTradeMessage(string message)
        {
            throw new NotImplementedException();
        }

        public override void OnTradeReady(bool ready)
        {
            throw new NotImplementedException();
        }

        public override void OnTradeAccept()
        {
            throw new NotImplementedException();
        }




        public override bool OnGroupAdd()
        {
            return false;
        }

        public override bool OnFriendAdd()
        {
            return false;
        }

        public override void OnFriendRemove() { }


        #endregion
    }
}
