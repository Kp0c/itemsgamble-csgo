using SteamKit2;
using System.Collections.Generic;
using SteamTrade;
using SteamTrade.TradeWebAPI;
using System.Threading.Tasks;
using System.Timers;

namespace SteamBot
{

    public class TraderBot
    { 
        public SteamID steamID;
        public string token;
        public TraderBot(SteamID steamID, string token)
        {
            this.steamID = steamID;
            this.token = token;
        }

    }
    public class SimpleUserHandler : UserHandler
    {
        public TF2Value AmountAdded;
        public bool autoStart = true;

        //cases
        public const string CHROMA = "720268538_0";
        public const string CHROMA_2 = "926978479_0";
        public const string HUNT = "469431148_0";
        public const string BREAKOUT = "520025252_0";

        //keys
        public const string CHROMA_KEY =    "721248158_143865972";
        public const string PHOENIX_KEY =   "360448780_143865972";
        public const string CSGO_KEY = "186150629_143865972";
        public const string BREAKOUT_KEY = "613589848_143865972";
        public const string HUNT_KEY = "506856210_143865972";
        public const string VANGUARD_KEY = "638243112_143865972";
        public const string WINTER_KEY = "259019412_143865972";
        public const string ESPORTS_KEY = "186150630_143865972";

        //consts
        public const float RATE = 7;
        public const int MAX_PRICE = 5;
        public const int MIN_PRICE = 3;

        public List<string> keys = new List<string>() { CHROMA_KEY, PHOENIX_KEY, CSGO_KEY, BREAKOUT_KEY, HUNT_KEY, VANGUARD_KEY, WINTER_KEY, ESPORTS_KEY, };

        public List<long> contextIds = new List<long>() { 2 };

        public GenericInventory myGenericInventory;
        public GenericInventory botGenericInventory;

        Timer timer = new Timer(10000);

        public List<TraderBot> TraderBots = new List<TraderBot>() { new TraderBot(new SteamID(76561198096366059),"RFE7GX8T"),
                                                                     new TraderBot(new SteamID(76561198096365603),"SuQMsCVr"), };
        

        public SimpleUserHandler (Bot bot, SteamID sid) : base(bot, sid) {}

        public override void OnNewTradeOffer(SteamTrade.TradeOffer.TradeOffer offer)
        {/*
            if (Bot.IsLoggedIn)
            {
                offer.Accept();
                if (offer.Items.GetMyItems().Count == 0)
                {
                    offer.Accept();
                    string msg = "� ������� ";
                    msg += offer.Items.GetTheirItems().Count;
                    msg += " ������";
                    GlobalVariables.casesGeted += offer.Items.GetTheirItems().Count;
                    Log.Success(msg);
                    Log.Success("����� � ������� " + GlobalVariables.casesGeted + " ������");
                }
                else
                {
                    GenericInventory theirGenericInventory = new GenericInventory(SteamWeb);
                    if (myGenericInventory == null) myGenericInventory = new GenericInventory(SteamWeb);

                    theirGenericInventory.load(730, contextIds, offer.PartnerSteamId);
                    myGenericInventory.load(730, contextIds, Bot.SteamClient.SteamID);

                    int keysC = 0;
                    int chromas = 0;

                    foreach (SteamTrade.TradeOffer.TradeOffer.TradeStatusUser.TradeAsset asset in offer.Items.GetTheirItems())
                        foreach (GenericInventory.Item item in theirGenericInventory.items.Values)
                            if (item.assetid == (ulong)asset.AssetId)
                                if (keys.Contains(item.descriptionid)) keysC++;

                    foreach (SteamTrade.TradeOffer.TradeOffer.TradeStatusUser.TradeAsset asset in offer.Items.GetMyItems())
                        foreach (GenericInventory.Item item in myGenericInventory.items.Values)
                            if (item.assetid == (ulong)asset.AssetId)
                                if (item.descriptionid == CHROMA) chromas++;
                    if (chromas != offer.Items.GetMyItems().Count)
                    {
                        Log.Error("���� �������� ��������?");
                    }
                    else
                        if (keysC != 0)
                        {
                            if ((float)chromas / keysC <= RATE)
                            {
                                offer.Accept();
                                Log.Success("I changed {0} chromas on {1} keys. Rate - {2}!", chromas, keysC, (float)chromas / keysC);
                            }
                            else
                            {
                                offer.Decline();
                                Log.Error("I not changed {0} chromas on {1} keys. Rate - {2}!", chromas, keysC, (float)chromas / keysC);
                            }
                        }
                }*/

                base.OnNewTradeOffer(offer);
            //}
        }

        public override bool OnGroupAdd()
        {
            return false;
        }

        public override bool OnFriendAdd () 
        {
            return false;
        }

        public override void OnLoginCompleted()
        {
            if (autoStart)
            {
                timer.AutoReset = true;
                timer.Start();
                timer.Elapsed += TimerTick;
                GlobalVariables.timerStarted = true;
                Log.Info("Auto start timer enabled");
            }
            if (myGenericInventory == null) myGenericInventory = new GenericInventory(SteamWeb);
        }

        public override void OnChatRoomMessage(SteamID chatID, SteamID sender, string message)
        {
            Log.Info(Bot.SteamFriends.GetFriendPersonaName(sender) + ": " + message);
            base.OnChatRoomMessage(chatID, sender, message);
        }

        public override void OnFriendRemove () {}

        public void SendBotOffer(int price, TraderBot otherTraderBot = null)
        {
            if (otherTraderBot == null) otherTraderBot = TraderBots[GlobalVariables.currentBot];
            SteamTrade.TradeOffer.TradeOffer offer;
            bool continueOffer = true;
            foreach (OfferIdWithCount offerId in GlobalVariables.offersBots)
            {
                Bot.TryGetTradeOffer(offerId.offerId, out offer);
                if (offer.PartnerSteamId == otherTraderBot.steamID)
                {
                    continueOffer = false;
                }
            }
            if (continueOffer)
            {
                //creating a new trade offer
                offer = Bot.NewTradeOffer(otherTraderBot.steamID);

                botGenericInventory = new GenericInventory(SteamWeb);
                if (myGenericInventory == null) myGenericInventory = new GenericInventory(SteamWeb);

                int Cases = 0;
                int maxCases = 0;

                myGenericInventory.load(730, contextIds, Bot.SteamClient.SteamID);
                botGenericInventory.load(730, contextIds, otherTraderBot.steamID);

                foreach (GenericInventory.Item item in myGenericInventory.items.Values)
                    if ((item.appid == 730) && (item.descriptionid == HUNT || item.descriptionid == BREAKOUT))
                        maxCases++;

                foreach (GenericInventory.Item item in botGenericInventory.items.Values)
                {
                    if (item.descriptionid == CHROMA && maxCases>=price)
                    {
                        offer.Items.AddTheirItem(730, item.contextid, (long)item.assetid);
                        Cases += price;
                        maxCases -= price;
                    }
                }
                if (Cases > 0)
                {
                    foreach (GenericInventory.Item item in myGenericInventory.items.Values)
                    {
                        if ((item.appid == 730) && (item.descriptionid == HUNT || item.descriptionid == BREAKOUT))
                        {
                            if (Cases >= 1)
                            {
                                offer.Items.AddMyItem(730, item.contextid, (long)item.assetid);
                                Cases -= 1;
                            }
                            else break;
                        }

                    }
                    if (Cases == 0)
                    {
                        string newOfferId;
                        if (offer.SendWithToken(out newOfferId, otherTraderBot.token, "no"))
                        {
                            Log.Success("Trade offer sent. Price: " + price);
                            GlobalVariables.offersBots.Add(new OfferIdWithCount(otherTraderBot, offer.Items.GetTheirItems().Count, price, newOfferId));
                        }
                    }
                    else
                    {
                        Log.Error("I don't have so much Cases! Please add {0} cases!", Cases);
                    }
                }
                //else Log.Error("Bot {0} haven't chroma.",offer.PartnerSteamId.ConvertToUInt64());
            }
            GlobalVariables.currentBot++;
            if (GlobalVariables.currentBot > TraderBots.Count - 1)
            {
                GlobalVariables.currentBot = 0;
            }

        }

        public void CheckOffers()
        {
            foreach (OfferIdWithCount offerId in GlobalVariables.offersBots)
            {
                SteamTrade.TradeOffer.TradeOffer offer;
                Bot.TryGetTradeOffer(offerId.offerId, out offer);
                if (offer.OfferState != SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateActive)
                {
                    if (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateAccepted)
                    {
                        Log.Success("� ������� {0} ������.", offerId.count);
                        GlobalVariables.offersBots.Remove(offerId);
                    }
                    else if ((offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateDeclined) ||
                      (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateCountered))
                    {
                        if (offerId.price <= MAX_PRICE)
                        {
                            Log.Error("��� {0} ������� ������, �������� ������� � ����� {1}", offer.PartnerSteamId, ++offerId.price);

                            GlobalVariables.offersBots.Remove(offerId);
                            SendBotOffer(offerId.price, offerId.traderBot);
                        }
                        else
                        {
                            Log.Error("��� {0} ����� ������ {1} �������� :/", offer.PartnerSteamId, MAX_PRICE);
                            GlobalVariables.offersBots.Remove(offerId);
                        }
                    }
                    else if (offer.OfferState == SteamTrade.TradeOffer.TradeOfferState.TradeOfferStateInvalidItems)
                    {
                        Log.Error("Invalid items");
                        GlobalVariables.offersBots.Remove(offerId);
                        SendBotOffer(offerId.price, offerId.traderBot);
                    }
                    else
                    {
                        Log.Error("offerId {0} state: {1}", offerId.offerId, offer.OfferState);
                    }
                }
            }
        }

        public void TimerTick(object source, ElapsedEventArgs e)
        {
            CheckOffers();
            SendBotOffer(MIN_PRICE);
            
        }

        public override void OnMessage(string message, EChatEntryType type, SteamID sender) 
        {

            switch (message)
            {
                case "buyed":
                    SendChatMessage("����� � ������� " + GlobalVariables.casesGeted + " ������");
                    break;
                case "test":
                    if (IsAdmin) SendBotOffer(MIN_PRICE);
                    break;
                case "Start":
                    if (!GlobalVariables.timerStarted)
                    {
                        timer.AutoReset = true;
                        timer.Start();
                        timer.Elapsed += TimerTick;
                        GlobalVariables.timerStarted = true;
                    }
                    else SendChatMessage("������ ��� �������");
                    break;
                case "Stop":
                    timer.Stop();
                    GlobalVariables.timerStarted = false;
                    break;
                case "chroma":
                    myGenericInventory = new GenericInventory(SteamWeb);
                    int Chroma = 0;
                    myGenericInventory.load(730, contextIds, Bot.SteamClient.SteamID);
                    foreach (GenericInventory.Item item in myGenericInventory.items.Values)
                    {
                        if (item.descriptionid == CHROMA)
                        {
                            Chroma += 1;
                        }
                    }
                    SendChatMessage("I have " + Chroma.ToString() + " chroma cases");
                    break;
            }
        }

            public override bool OnTradeRequest() 
            {
                return true;
            }
        
            public override void OnTradeError (string error) 
            {
                SendChatMessage("Oh, there was an error: {0}.", error);
                Log.Warn (error);
            }
        
            public override void OnTradeTimeout () 
            {
                SendChatMessage("Sorry, but you were AFK and the trade was canceled.");
                Log.Info ("User was kicked because he was AFK.");
            }
        
            public override void OnTradeInit() 
            {
               SendTradeMessage("Success. Please put up your items.");
            }
        
            public override void OnTradeAddItem (CSGOSchema.Item schemaItem, Inventory.Item inventoryItem) {
                //Log.Success(schemaItem.ItemTypeName);
               // SendTradeMessage(schemaItem.ItemName);
            }
        
            public override void OnTradeRemoveItem (CSGOSchema.Item schemaItem, Inventory.Item inventoryItem) {}
        
            public override void OnTradeMessage (string message) {}
        
            public override void OnTradeReady (bool ready) 
            {
                /*if (!ready)
                {
                    Trade.SetReady (false);
                }
                else
                {
                    if(Validate ())
                    {
                        Trade.SetReady (true);
                    }
                    SendTradeMessage("Scrap: {0}", AmountAdded.ScrapTotal);
                }*/
            }

            public override void OnTradeSuccess()
            {
                // Trade completed successfully
                Log.Success("Trade Complete.");
            }


            public override void OnTradeAccept() 
            {
                /*if (Validate() || IsAdmin)
                {
                    //Even if it is successful, AcceptTrade can fail on
                    //trades with a lot of items so we use a try-catch
                    try {
                        if (Trade.AcceptTrade())
                            Log.Success("Trade Accepted!");
                    }
                    catch {
                        Log.Warn ("The trade might have failed, but we can't be sure.");
                    }
                }*/
            }

        public bool Validate ()
        {            
            /*AmountAdded = TF2Value.Zero;
            
            List<string> errors = new List<string> ();
            
            foreach (TradeUserAssets asset in Trade.OtherOfferedItems)
            {
                var item = Trade.OtherInventory.GetItem(asset.assetid);
                if (item.Defindex == 5000)
                    AmountAdded += TF2Value.Scrap;
                else if (item.Defindex == 5001)
                    AmountAdded += TF2Value.Reclaimed;
                else if (item.Defindex == 5002)
                    AmountAdded += TF2Value.Refined;
                else
                {
                    var schemaItem = Trade.CurrentSchema.GetItem (item.Defindex);
                    errors.Add ("Item " + schemaItem.Name + " is not a metal.");
                }
            }
            
            if (AmountAdded == TF2Value.Zero)
            {
                errors.Add ("You must put up at least 1 scrap.");
            }
            
            // send the errors
            if (errors.Count != 0)
                SendTradeMessage("There were errors in your trade: ");
            foreach (string error in errors)
            {
                SendTradeMessage(error);
            }
            */
            return false;
        }
        
    }
 
}

