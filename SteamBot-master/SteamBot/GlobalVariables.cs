﻿using SteamKit2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamBot
{
    static class GlobalVariables
    {
       public static int casesGeted = 0;
       public static int currentBot = 0;
       public static List<OfferIdWithCount> offersBots = new List<OfferIdWithCount>();
       public static Dictionary<SteamID, int> chatUsers = new Dictionary<SteamID, int>();
       public static bool timerStarted = false;

       public static void ResetChatUsers()
       {
           chatUsers = new Dictionary<SteamID, int>();
       }
    }

    public class OfferIdWithCount
    {
        public int count;
        public int price;
        public TraderBot traderBot;
        public string offerId;

        public OfferIdWithCount(TraderBot traderBot, int count, int price, string offerId)
        {
            this.price = price;
            this.offerId = offerId;
            this.traderBot = traderBot;
            this.count = count;
        }
    }
}
